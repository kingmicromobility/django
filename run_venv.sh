#!/bin/bash

cd `dirname $0`

. ./venv/bin/activate

set -a
. ./.env
set +a

"$@"
