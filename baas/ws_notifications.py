import os
from pusher import Pusher


def pusher_connection():
    if os.environ.get('PUSHER_APP_ID'):
        return Pusher(
            app_id=os.environ.get('PUSHER_APP_ID'),
            key=os.environ.get('PUSHER_KEY'),
            secret=os.environ.get('PUSHER_SECRET'))


def bike_status(bike):
    pusher = pusher_connection()
    if not pusher:
        return
    channel_name = u'market-'
    if bike.market:
        channel_name += str(bike.market.guid)
    event_name = u'bike'
    event_data = {
        u'guid': bike.guid,
        u'name': bike.name,
        u'ambassador': str(bike.ambassador.guid),
        u'brand': {
            u'name': bike.brand.name},
        u'style': {
            u'name': bike.style.name},
        u'size': {
            u'name': bike.size.name},
        u'status': bike.status,
        u'last_location': str(bike.last_location),
        u'last_lat': bike.last_latitude(),
        u'last_lon': bike.last_longitude(),
        u'detail_set': bike.details(),
        u'lock': {
            'guid': bike.lock.guid,
            'type': bike.lock.type,
            'baas_lock_id': bike.lock.baas_lock_id,
            'urn': bike.lock.urn(),
            'battery_remaining': bike.lock.battery_remaining
        },
        u'unique_renter_count': bike.unique_renter_count()
    }
    pusher.trigger(channel_name, event_name, event_data)


def session_status(session):
    pusher = pusher_connection()
    if not pusher:
        return
    channel_name = u'market-'
    if session.bike.market:
        channel_name += str(session.bike.market.guid)
    event_name = u'session'
    event_data = {
        u'guid': str(session.guid),
        u'bike': {
            u'guid': str(session.bike.guid),
            u'name': session.bike.name,
            u'status': session.bike.status,
            u'last_location': str(session.bike.last_location)
        },
        u'renter': {
            u'first_name': session.renter.first_name,
            u'last_name': session.renter.last_name
        },
        u'trip_set': serialized_trips_as_list(session.trip_set.all())
    }
    # pusher.trigger(channel_name, event_name, event_data)


def serialized_trips_as_list(trips):
    return list(map((lambda t: {
        u'guid': str(t.guid),
        u'route': t.route
    }), trips))
