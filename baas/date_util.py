from datetime import timedelta, date


def date_list(start, end):
    num_days = (end - start).days
    date_range = range((date.today() - end).days, num_days)
    return [end - timedelta(days=days_ago) for days_ago in date_range]
