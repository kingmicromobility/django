__author__ = 'johngazzini'


def googleMapsURLFromLocation(location):
    if location is None:
        return None
    else:
        return 'https://maps.google.com/maps?q=%s,%s' % (location.y, location.x)


def googleMapsLinkFromLocation(location):
    url = googleMapsURLFromLocation(location)
    if url is None:
        return None
    else:
        return u"<a href='%s' target='_blank'>Google Maps</a>" % url
