import os
import re
import uuid
import django_rq
import requests
import tinys3
from datetime import timedelta
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.db import models
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos.point import Point
from django.contrib.gis.measure import Distance as D
from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.contrib.auth import get_user_model
from django_rq import job
from baas_backend import settings
from baas import mailers, ws_notifications, mailing_list_manager, date_util, payment_gateway
from baas.static_map import GoogleStaticMap, GoogleStaticMapMarker, GoogleStaticMapPolyline

lat_lon_reference = SpatialReference(4326)
spheroid_reference = SpatialReference(3857)
lat_lon_to_spheroid = CoordTransform(lat_lon_reference, spheroid_reference)
spheroid_to_lat_lon = CoordTransform(spheroid_reference, lat_lon_reference)


class BaasBaseObject(models.Model):
    class Meta:
        abstract = True

    guid = models.CharField(
        primary_key=True, max_length=40, blank=True, db_index=True)
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    last_modified = models.DateTimeField(
        blank=True, null=True, auto_now=True, db_index=True)
    deleted = models.BooleanField(default=False)

    def is_first_save(self):
        return not self.pk

    def save(self, *args, **kwargs):
        self.full_clean()
        if not self.guid:
            self.guid = uuid.uuid4()
        super(BaasBaseObject, self).save(*args, **kwargs)


class Authority(BaasBaseObject):
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class BikeImageSet(BaasBaseObject):
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class BikeImage(BaasBaseObject):
    IMAGE_KEYS = (('thumbnail', 'thumbnail'),)
    set = models.ForeignKey(BikeImageSet, on_delete=models.CASCADE, blank=False, null=False)
    key = models.CharField(
        max_length=255, blank=False, null=False, choices=IMAGE_KEYS,
        default='thumbnail')
    name = models.CharField(max_length=255, blank=False, null=False)
    image_uri = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Market(BaasBaseObject):
    MARKET_STATES = (('pre-launch', 'pre-launch'),
                     ('active', 'active'),
                     ('closed', 'closed'))
    name = models.CharField(max_length=255, blank=False, null=False)
    geoshape = gis_models.MultiPolygonField(srid=4326, blank=False, null=False)
    state = models.CharField(
        max_length=255, blank=False, null=False, choices=MARKET_STATES,
        default='pre-launch')
    bike_image_sets = models.ManyToManyField(
        BikeImageSet,
        through='Bike',
        through_fields=('market', 'image_set'))

    def contains(self, point):
        return point.intersects(self.geoshape)

    def attribution_channels(self):
        channels = AttributionChannel.objects.filter(
            models.Q(active=True)
            & (models.Q(market=self) | models.Q(market__isnull=True))).all()
        return sorted(channels, key=lambda channel: channel.name)

    def get_closest_bike(self, point):
        return self.bike_set.filter(status='available').annotate(
            distance=Distance('last_location', point)
                ).order_by('distance').first()

    def get_closest_bike_distance(self, point):
        closest_bike = self.get_closest_bike(point)
        if closest_bike:
            closest_bike.last_location.transform(lat_lon_to_spheroid)
            point.transform(lat_lon_to_spheroid)
            return D(m=closest_bike.last_location.distance(point)).mi
        else:
            return None

    def __str__(self):
        return self.name


class Person(BaasBaseObject):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    paying_customer = models.BooleanField(default=True)
    payment_gateway_id = models.CharField(
        max_length=255, blank=True, null=True)
    original_market = models.ForeignKey(
        Market, on_delete=models.CASCADE, related_name='people_who_first_appeared_here',
        blank=True, null=True)
    last_market = models.ForeignKey(
        Market, on_delete=models.CASCADE, related_name='people_who_last_appeared_here',
        blank=True, null=True)

    class Meta:
        ordering = ('first_name', 'last_name',)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def marketing_channel_attributions_all(self):
        return set(list(self.marketing_channel_attributions_self_reported()) +
                   list(self.marketing_channel_attributions_auto_reported()))

    def marketing_channel_attributions_self_reported(self):
        return self.attribution_auto_reported.all()

    def marketing_channel_attributions_auto_reported(self):
        return self.attribution_self_reported.all()

    def open_sessions_for_bike(self, bike):
        return self.session_set.filter(
            bike=bike, started_at__lte=timezone.now(), ended_at=None)

    def has_open_reservations_or_sessions(self):
        now = timezone.now()
        num_open_reservations = self.reservation_set.filter(
            started_at__lte=now, expires_at__gte=now, canceled_at=None,
            session_started_at=None).count()
        num_open_sessions = self.session_set.filter(ended_at=None).count()
        return (num_open_reservations + num_open_sessions) > 0

    def open_reservations_for_bike(self, bike):
        now = timezone.now()
        return self.reservation_set.filter(
            bike=bike, started_at__lte=now, expires_at__gte=now,
            canceled_at=None, session_started_at=None)

    def has_open_sessions_or_reservations_for_bike(self, bike):
        reservations = self.open_reservations_for_bike(bike).count()
        sessions = self.open_sessions_for_bike(bike).count()
        return reservations + sessions > 0

    def significant_sessions(self):
        return self.session_set.filter(
            Q(total_distance_miles__gte=0.1) |
            Q(total_duration_seconds__gte=300))

    def first_significant_session(self):
        return self.significant_sessions().order_by('created_at')[0]

    def first_repeat_session(self):
        return self.significant_sessions().order_by('created_at')[1]

    def is_active(self):
        return self.significant_sessions().count() > 0

    def is_repeat(self):
        return self.significant_sessions().count() > 1

    def reset_password(self):
        password = get_user_model().objects.make_random_password()
        self.user.set_password(password)
        self.user.save()
        mailers.send_password_reset_email(self, password)

    def save(self, *args, **kwargs):
        is_first_save = not self.pk
        self.payment_gateway_id = payment_gateway.create_or_update_customer(
            self)
        super(Person, self).save(*args, **kwargs)
        if is_first_save:
            mailers.send_welcome_email.delay(self)
            mailing_list_manager.add_person_to_list(
                os.environ.get('MAILCHIMP_USERS_LIST'),
                self.user.email,
                self.first_name,
                self.last_name,
                self.phone_number,
                'University of Maryland',
                False)


class AmbassadorDetail(BaasBaseObject):
    name = models.CharField(max_length=255, blank=True, null=True)
    person = models.OneToOneField(
        Person, on_delete=models.CASCADE, related_name='ambassador_person', blank=False, null=False)
    market = models.ForeignKey(
        Market, on_delete=models.CASCADE, related_name='ambassador_market', blank=False, null=False)

    def __str__(self):
        return self.name or str(self.person)


class AttributionChannel(BaasBaseObject):
    name = models.CharField(max_length=255, blank=False, null=False)
    descriptive_phrase = models.CharField(
        max_length=255, blank=False, null=False)
    market = models.ForeignKey(
        Market, on_delete=models.CASCADE, related_name='attribution_market', blank=True, null=True)
    ambassador_details = models.ForeignKey(
        AmbassadorDetail, on_delete=models.CASCADE, related_name='attribution_ambassador', blank=True,
        null=True)
    attribution_code = models.CharField(max_length=255, blank=True, null=True)
    branch_url = models.CharField(max_length=255, blank=True, null=True)
    self_reported_people = models.ManyToManyField(
        Person, related_name='attribution_self_reported', blank=True)
    auto_reported_people = models.ManyToManyField(
        Person, related_name='attribution_auto_reported', blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return '%s - %s' % (self.name, self.descriptive_phrase)

    def attributed_people(self):
        self_reported = list(self.self_reported_people.all())
        auto_reported = list(self.auto_reported_people.all())
        return set(self_reported + auto_reported)

    def attributed_people_by_day(self, start_date, end_date):
        people = self.attributed_people()
        results = {}
        for day in date_util.date_list(start_date, end_date):
            created_this_day = lambda person: day == person.created_at.date()
            results[str(day)] = len(list(filter(created_this_day, people)))
        return results

    def attributed_people_with_payment_method(self):
        self_reported = list(self.self_reported_people.filter(
            payment_gateway_id__isnull=False))
        auto_reported = list(self.auto_reported_people.filter(
            payment_gateway_id__isnull=False))
        return set(self_reported + auto_reported)

    def attributed_people_with_payment_method_by_day(self, start_date, end_date):
        people = self.attributed_people_with_payment_method()
        results = {}
        for day in date_util.date_list(start_date, end_date):
            created_this_day = lambda person: day == person.created_at.date()
            results[str(day)] = len(list(filter(created_this_day, people)))
        return results

    def attributed_active_riders(self):
        people = self.attributed_people_with_payment_method()
        return [person for person in people if person.is_active()]

    def attributed_active_riders_by_day(self, start_date, end_date):
        people = self.attributed_active_riders()
        results = {}
        for day in date_util.date_list(start_date, end_date):
            activated_this_day = lambda person: day == person.first_significant_session().created_at.date()
            results[str(day)] = len(list(filter(activated_this_day, people)))
        return results

    def attributed_repeat_riders(self):
        people = self.attributed_people_with_payment_method()
        return [person for person in people if person.is_repeat()]

    def attributed_repeat_riders_by_day(self, start_date, end_date):
        people = self.attributed_repeat_riders()
        results = {}
        for day in date_util.date_list(start_date, end_date):
            activated_this_day = lambda person: day == person.first_repeat_session().created_at.date()
            results[str(day)] = len(list(filter(activated_this_day, people)))
        return results


class BikeBrand(BaasBaseObject):
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class BikeSize(BaasBaseObject):
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class BikeStyle(BaasBaseObject):
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class Lock(BaasBaseObject):
    type = models.CharField(max_length=255, blank=False, null=False)
    baas_lock_id = models.CharField(max_length=255, blank=False, null=False)
    salt = models.CharField(max_length=255, blank=True, null=True)
    battery_remaining = models.FloatField()

    @classmethod
    def find_by_urn(cls, urn):
        pattern = re.compile('baas\.lock\.([\w\d\-\_:]+).([\w\d\-\_:]+)')
        match = pattern.match(urn)
        return Lock.objects.get(type=match.group(1),
                                baas_lock_id=match.group(2))

    def urn(self):
        return 'baas.lock.%s.%s' % (self.type, self.baas_lock_id)

    def hash(self, challenge_code):
        return (challenge_code * (challenge_code + 3)) % int(self.salt)

    def answer_code(self, person, challenge_code):
        if person.has_open_sessions_or_reservations_for_bike(self.bike):
            return self.hash(challenge_code)
        else:
            return -1

    def __str__(self):
        return self.urn()


class Bike(BaasBaseObject):
    BIKE_STATUSES = (('under assembly', 'under assembly'),
                     ('ready for delivery', 'ready for delivery'),
                     ('available', 'available'),
                     ('reserved', 'reserved'),
                     ('active', 'active'),
                     ('held', 'held'),
                     ('sold', 'sold'),
                     ('awaiting inspection', 'awaiting inspection'),
                     ('awaiting on-site maintenance', 'awaiting on-site maintenance'),
                     ('under on-site maintenance', 'under on-site maintenance'),
                     ('awaiting off-site maintenance', 'awaiting off-site maintenance'),
                     ('under off-site maintenance', 'under off-site maintenance'),
                     ('missing', 'missing'),
                     ('retired', 'retired'),
                     ('demo only', 'demo only'),
                     ('under_maintenance', 'under_maintenance'),
                     ('out_of_fleet', 'out_of_fleet'))
    LISTING_TYPES = (('for rent', 'for rent'),
                     ('for sale', 'for sale'))
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(
        Person, on_delete=models.CASCADE, related_name='bike_owned_set', blank=True, null=True)
    ambassador = models.ForeignKey(
        Person, on_delete=models.CASCADE, related_name='bike_ambassador_set', blank=True, null=True)
    image_set = models.ForeignKey(BikeImageSet, on_delete=models.CASCADE, blank=True, null=True)
    brand = models.ForeignKey(
        BikeBrand, on_delete=models.CASCADE, related_name='bike_brand', blank=True, null=True)
    style = models.ForeignKey(
        BikeStyle, on_delete=models.CASCADE, related_name='bike_style', blank=True, null=True)
    size = models.ForeignKey(
        BikeSize, on_delete=models.CASCADE, related_name='bike_size', blank=True, null=True)
    status = models.CharField(
        max_length=255, blank=False, null=False, choices=BIKE_STATUSES,
        default='out_of_fleet')
    owner_available = models.BooleanField(default=True)
    baas_available = models.BooleanField(default=True)
    launch_date = models.DateTimeField(blank=True, null=True)
    current_renter = models.ForeignKey(
        Person, on_delete=models.CASCADE, related_name='currently_rented_bikes_set',
        blank=True, null=True)
    last_location = gis_models.PointField(blank=True, null=True, srid=4326)
    market = models.ForeignKey(Market, on_delete=models.CASCADE, blank=True, null=True)
    lock = models.OneToOneField(Lock, on_delete=models.CASCADE, blank=True, null=True)
    listing_type = models.CharField(
        max_length=255, blank=False, null=False, choices=LISTING_TYPES,
        default='for rent')

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    def last_latitude(self):
        return self.last_location.y if self.last_location is not None else None

    def last_longitude(self):
        return self.last_location.x if self.last_location is not None else None

    def reservation_opened(self, reservation):
        self.status = 'reserved'
        self.current_renter = reservation.renter
        self.save()

    def reservation_closed(self):
        self.status = 'available'
        self.save()

    def reservation_expired(self):
        self.status = 'available'
        self.current_renter = None
        self.save()

    def session_active(self, session):
        self.status = 'active'
        self.current_renter = session.renter
        self.save()

    def session_held(self, session):
        self.status = 'held'
        self.current_renter = session.renter
        self.save()

    def session_sold(self, session):
        self.status = 'sold'
        self.current_renter = None
        self.baas_available = False
        self.owner = session.renter
        self.save()

    def session_closed(self):
        self.status = 'available'
        self.current_renter = None
        self.save()

    def unique_renter_count(self):
        return 0
        # return self.session_set.values('renter').annotate(
        #     total=models.Count('renter')).count()

    def update_last_location(self, new_location):
        self.last_location = new_location
        self.save()

    def within_own_market(self):
        return self.last_location.intersects(self.market.geoshape)

    def details(self):
        details = {}
        for detail in self.detail_set.all():
            details[detail.name] = detail.value
        return details

    def get_sell_price(self):
        details = self.details()
        if 'price_with_lock' in details:
            try:
                sell_price = float(details['price_with_lock'])
                # express price in pennies
                return int(sell_price * 100)
            except (ValueError, AttributeError) as e:
                raise ValidationError({'details_set.price_with_lock': str(e)})
        else:
            return 0

    def save(self, ws_notification=True, *args, **kwargs):
        super(Bike, self).save(*args, **kwargs)
        if ws_notification:
            ws_notifications.bike_status(self)


class BikeDetail(BaasBaseObject):
    bike = models.ForeignKey(
        Bike, on_delete=models.CASCADE, related_name='detail_set', blank=False, null=False)
    name = models.CharField(max_length=255)
    value = models.CharField(max_length=255)

    def __str__(self):
        return '(%s) %s : %s' % (self.bike, self.name, self.value)


class FinancialTransaction(BaasBaseObject):
    description = models.CharField(max_length=255, blank=False, null=False)
    total_amount = models.IntegerField(default=0,blank=False, null=False)
    paid_in_full = models.BooleanField(default=False, blank=False, null=False)

    def add_line_item(self, description, amount):
        self.line_item_set.create(
            description=description, amount=amount)
        self.recalculate_total_amount()

    def attempt_payment(self, person, delayed=True):
        payment_attempt = self.payment_attempt_set.create(
            amount=self.total_amount)
        payment_attempt.attempt_payment(person, delayed)

    def recalculate_total_amount(self):
        total = 0
        for line_item in self.line_item_set.all():
            total += line_item.amount
        self.total_amount = total
        self.save()


class FinancialTransactionLineItem(BaasBaseObject):
    description = models.CharField(max_length=255, blank=False, null=False)
    amount = models.IntegerField(blank=False, null=False)
    transaction = models.ForeignKey(
        FinancialTransaction, on_delete=models.CASCADE, blank=False, null=False,
        related_name='line_item_set')


class FinancialTransactionPaymentAttempt(BaasBaseObject):
    payment_method_description = models.CharField(
        max_length=255, blank=True, null=True)
    payment_method_card_type = models.CharField(
        max_length=255, blank=True, null=True)
    payment_method_last_4 = models.CharField(
        max_length=255, blank=True, null=True)
    amount = models.IntegerField(blank=False, null=False)
    result = models.CharField(max_length=255, blank=True, null=True)
    successful = models.BooleanField(default=False, blank=False, null=False)
    attempted_at = models.DateTimeField(blank=True, null=True)
    transaction = models.ForeignKey(
        FinancialTransaction, on_delete=models.CASCADE, blank=False, null=False,
        related_name='payment_attempt_set')

    def attempt_payment(self, person, delayed=True):
        if delayed:
            payment_gateway.create_transaction.delay(person, self)
        else:
            payment_gateway.create_transaction(person, self)


class LockEvent(BaasBaseObject):
    LOCK_EVENT_TYPES = (('locked', 'locked'), ('unlocked', 'unlocked'))
    event_type = models.CharField(max_length=25, choices=LOCK_EVENT_TYPES)
    bike = models.ForeignKey(Bike, on_delete=models.CASCADE, blank=False, null=False)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=False, null=False)
    time = models.DateTimeField(blank=False, null=False, auto_now_add=True)
    location = gis_models.PointField(blank=False, null=False, srid=4326)
    outside_market = models.BooleanField(
        default=False, blank=False, null=False)
    lock = models.ForeignKey(Lock, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        f = '%s %s %s at %s near %s'
        v = (self.person, self.event_type, self.bike, self.time, self.location)
        return f % v

    def clean(self):
        self.outside_market = not self.bike.within_own_market()
        if self.event_type == 'unlocked':
            self.validate_unlock()
        elif self.event_type == 'locked':
            self.validate_lock()

    def validate_unlock(self):
        open_reservation = self.person.open_reservations_for_bike(
            self.bike).first()
        open_session = self.person.open_sessions_for_bike(self.bike).first()
        if not open_session and not open_reservation:
            raise ValidationError(
                {'non_field_errors':
                    'No session or reservation for user and bike.'})

    def validate_lock(self):
        open_session = self.person.open_sessions_for_bike(self.bike).first()
        if not open_session or not open_session.open_trip():
            raise ValidationError(
                {'non_field_errors': 'No trip in progress.'})

    def update_session_and_trip(self):
        if self.event_type == 'unlocked':
            self.update_session_and_trip_on_unlock()
        elif self.event_type == 'locked':
            self.update_session_and_trip_on_lock()

    def update_session_and_trip_on_unlock(self):
        open_reservation = self.person.open_reservations_for_bike(
            self.bike).first()
        open_session = self.person.open_sessions_for_bike(self.bike).first()
        if open_session:
            open_session.begin_trip(unlock_event=self)
        elif open_reservation:
            new_session = open_reservation.session_set.create(
                renter=self.person, bike=self.bike, started_event=self)
            new_session.begin_trip(self)
        else:
            raise InvalidSessionStateException(
                message='No session or reservation for user and bike.')

    def update_session_and_trip_on_lock(self):
        open_session = self.person.open_sessions_for_bike(self.bike).first()
        open_session.end_trip(self)

    def get_trip(self):
        if self.event_type == 'unlocked':
            return self.started_trip
        elif self.event_type == 'locked':
            return self.ended_trip
        else:
            return None

    def associated_session_guid(self):
        session = self.person.session_set.order_by('created_at').last()
        return session.guid

    def save(self, *args, **kwargs):
        self.lock = self.bike.lock
        self.bike.update_last_location(self.location)
        super(LockEvent, self).save(*args, **kwargs)
        self.update_session_and_trip()


class Reservation(BaasBaseObject):
    bike = models.ForeignKey(Bike, on_delete=models.CASCADE, blank=False, null=False)
    market = models.ForeignKey(Market, on_delete=models.CASCADE, blank=False, null=False)
    renter = models.ForeignKey(Person, on_delete=models.CASCADE, blank=False, null=False)
    location = gis_models.PointField(blank=True, null=True, srid=4326)
    started_at = models.DateTimeField(blank=False, null=False)
    expires_at = models.DateTimeField(blank=False, null=False)
    canceled_at = models.DateTimeField(blank=True, null=True)
    session_started_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        fstr = 'Reservation of "%s" by renter "%s" at "%s" initiated near "%s"'
        values = (self.bike, self.renter, self.started_at, self.location)
        return fstr % values

    def cancel(self):
        self.canceled_at = timezone.now()
        self.bike.reservation_closed()
        self.save()

    def populate_reservation_term(self):
        if self.started_at is None:
            self.started_at = timezone.now()
        if self.started_at is not None and self.expires_at is None:
            self.expires_at = self.started_at + timedelta(minutes=15)

    def start_session(self, session):
        self.session_started_at = session.started_event.time
        self.save()

    def expire(self):
        if not self.canceled_at and not self.session_started_at:
            self.bike.reservation_expired()

    def save(self, *args, **kwargs):
        if not self.pk and self.bike.status != 'available':
            raise ValidationError(
                {'non_field_errors': 'Bike is not available for reservation.'})
        if not self.pk and self.renter.has_open_reservations_or_sessions():
            raise ValidationError(
                {'non_field_errors':
                    'Renter already has active reservation or session.'})
        else:
            self.populate_reservation_term()
            self.market = self.bike.market
            if not self.pk:
                self.bike.reservation_opened(self)
            super(Reservation, self).save(*args, **kwargs)
            scheduler = django_rq.get_scheduler('default')
            scheduler.enqueue_at(
                self.expires_at, reservation_expiration_job,
                reservation_guid=str(self.guid))


def reservation_expiration_job(reservation_guid):
    reservation = Reservation.objects.get(guid=reservation_guid)
    reservation.expire()


class Session(BaasBaseObject):
    bike = models.ForeignKey(Bike, on_delete=models.CASCADE, blank=False, null=False)
    market = models.ForeignKey(Market, on_delete=models.CASCADE, blank=False, null=False)
    renter = models.ForeignKey(
        Person, on_delete=models.CASCADE, blank=False, null=False, related_name='session_set')
    owner = models.ForeignKey(
        Person, on_delete=models.CASCADE, blank=True, null=True, related_name='session_bike_owner_set')
    ambassador = models.ForeignKey(
        Person, on_delete=models.CASCADE, blank=True, null=True,
        related_name='session_bike_ambassador_set')
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE, blank=False, null=False)
    started_event = models.OneToOneField(
        LockEvent, on_delete=models.CASCADE, blank=False, null=False,
        related_name='session_started_lock_event')
    started_at = models.DateTimeField(blank=False, null=False)
    ended_event = models.OneToOneField(
        LockEvent, on_delete=models.CASCADE, blank=True, null=True,
        related_name='session_ended_lock_event')
    ended_at = models.DateTimeField(blank=True, null=True)
    end_outside_market = models.BooleanField(
        default=False, blank=False, null=False)
    end_by_purchase = models.BooleanField(
        default=False, blank=False, null=False)
    total_distance_miles = models.FloatField(
        default=0.0, blank=False, null=False)
    total_duration_seconds = models.IntegerField(
        default=0, blank=False, null=False)
    financial_transaction = models.OneToOneField(
        FinancialTransaction, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        formatstr = 'Session with bike "%s" by renter "%s" at "%s"'
        values    = (self.bike, self.renter, self.started_at)
        return formatstr % values

    def capture_owner_data(self):
        self.owner = self.bike.owner
        self.ambassador = self.bike.ambassador

    def compute_rental_price(self):
        session_time = self.ended_at - self.started_at
        hours  = session_time.days * 24
        hours += session_time.seconds // 3600
        hours += 1 if session_time.seconds % 3600 > 0 else 0
        rental_price = hours * 100  # express price in pennies
        return rental_price

    def compute_out_of_bounds_fee(self):
        out_of_bounds_fee = 0
        if self.end_outside_market:
            out_of_bounds_fee = 2500
        return out_of_bounds_fee

    def compute_total_distance(self):
        dist = 0.0
        for trip in self.trip_set.all():
            dist += trip.distance_miles
        return dist

    def compute_total_duration(self):
        duration = 0
        for trip in self.trip_set.all():
            duration += trip.duration_seconds
        return duration

    def open_trip(self):
        return self.trip_set.filter(ended_event=None).first()

    def begin_trip(self, unlock_event):
        if self.open_trip():
            raise InvalidTripStateException(
                message='Trip already in progress.')
        else:
            self.bike.session_active(self)
            self.trip_set.create(started_event=unlock_event)

    def end_trip(self, lock_event):
        self.open_trip().end(lock_event)
        self.bike.session_held(self)

    def end_session(self):
        if self.bike.within_own_market() or self.end_outside_market:
            self.ended_at = timezone.now()
            self.save()
            self.bike.session_closed()
            self.create_and_complete_financial_transaction()
            generate_session_route_map_job.delay(self.guid)
        else:
            raise BikeOutsideMarketException(
                {'instruction': """Bike is outside market boundary. Fee will
                apply. Pass 'end_outside_market'='True' to end session and
                incur fee."""})

    def end_session_by_purchase(self):
        if self.bike.get_sell_price() <= 0:
            raise ValidationError(
                {'non_field_errors': 'Bike is not available for sale.'})
        self.ended_at = timezone.now()
        self.create_and_complete_purchase_financial_transaction()
        self.save()
        self.bike.session_sold(self)
        generate_session_route_map_job.delay(self.guid)

    def create_rental_price_line_item(self):
        rental_price = self.compute_rental_price()
        if rental_price > 0:
            self.financial_transaction.add_line_item(
                'Hourly Rental', rental_price)

    def create_out_of_bounds_line_item(self):
        out_of_bounds_fee = self.compute_out_of_bounds_fee()
        if out_of_bounds_fee > 0:
            self.financial_transaction.add_line_item(
                'Out-of-Bounds Fee', out_of_bounds_fee)

    def create_and_complete_financial_transaction(self):
        description_template = 'Rental - %s %s, %s, %s'
        description_values = (
            self.renter.first_name,
            self.renter.last_name,
            self.bike.name,
            self.ended_at)
        self.financial_transaction = FinancialTransaction.objects.create(
            description=description_template % description_values)
        self.create_rental_price_line_item()
        self.create_out_of_bounds_line_item()
        self.financial_transaction.attempt_payment(self.renter)

    def create_and_complete_purchase_financial_transaction(self):
        description_template = 'Purchase - %s %s, %s, %s'
        description_values = (
            self.renter.first_name,
            self.renter.last_name,
            self.bike.name,
            self.ended_at)
        self.financial_transaction = FinancialTransaction.objects.create(
            description=description_template % description_values)
        line_item_descr = '%s %s, size %s' % (self.bike.brand, self.bike.name, self.bike.size)
        self.financial_transaction.add_line_item(
            line_item_descr, self.bike.get_sell_price())
        try:
            # raise error if purchase payment fails
            self.financial_transaction.attempt_payment(self.renter, delayed=False)
        except ValidationError:
            self.financial_transaction.deleted = True
            self.financial_transaction.save()
            raise

    def save(self, *args, **kwargs):
        creating = not self.pk
        self.capture_owner_data()
        self.market = self.bike.market
        self.total_distance_miles = self.compute_total_distance()
        self.total_duration_seconds = self.compute_total_duration()
        if self.started_at is None and self.started_event:
            self.started_at = self.started_event.time
        super(Session, self).save(*args, **kwargs)
        if creating:
            self.reservation.start_session(self)

    def get_s3_path_to_route_image(self):
        guid = self.guid
        time = self.started_at.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        return 'sessions/%s-%s/route_map.png' % (time, guid)

    def static_map_url(self, width, height):
        api_key = os.environ.get('GOOGLE_STATIC_MAPS_API_KEY')
        static_map = GoogleStaticMap(api_key)
        static_map.set_size(width, height)
        static_map.add_marker(self.get_start_marker())
        static_map.add_marker(self.get_end_marker())
        static_map.add_marker(self.get_hold_markers())
        static_map.add_polyline(self.get_route_polyline())
        return static_map.as_url()

    def get_start_marker(self):
        size = 'large'
        color = 'green'
        label = 'S'
        location = self.trip_set.first().starting_point()
        location_str = '%s,%s' % (location.y, location.x)
        return GoogleStaticMapMarker(size, color, label, [location_str])

    def get_end_marker(self):
        size = 'large'
        color = 'red'
        label = 'E'
        location = self.trip_set.last().ending_point()
        location_str = '%s,%s' % (location.y, location.x)
        return GoogleStaticMapMarker(size, color, label, [location_str])

    def get_hold_markers(self):
        size = 'small'
        color = 'yellow'
        label = 'H'
        marker = GoogleStaticMapMarker(size, color, label, [])
        for trip in self.trip_set.all():
            location = trip.ending_point()
            marker.add_location('%s,%s' % (location.y, location.x))
        return marker

    def get_route_polyline(self):
        weight = 7
        color = 'blue'
        polyline = GoogleStaticMapPolyline(weight, color, [])
        for trip in self.trip_set.all():
            for entry in trip.route_entries():
                polyline.add_location((entry[1], entry[2]))
        return polyline

@job('default', result_ttl=0)
def generate_session_route_map_job(session_guid):
    session = Session.objects.get(guid=session_guid)
    image_url = session.static_map_url(400,400)
    static_map_response = requests.get(image_url)
    tmp_file_name = 'tmp_map_image.png'
    with open(tmp_file_name,'wb') as handle:
        handle.write(static_map_response.content)
        handle.close()
    s3_connection = tinys3.Connection(
        os.environ.get('S3_ACCESS_KEY'),
        os.environ.get('S3_SECRET'))
    with open(tmp_file_name,'rb') as handle:
        s3_connection.upload(
            session.get_s3_path_to_route_image(),
            handle,
            os.environ.get('S3_STATICS_BUCKET'))
    os.remove(tmp_file_name)


class Trip(BaasBaseObject):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    started_event = models.OneToOneField(
        LockEvent, on_delete=models.CASCADE, related_name='started_trip', blank=False, null=False)
    ended_event = models.OneToOneField(
        LockEvent, on_delete=models.CASCADE, related_name='ended_trip', blank=True, null=True)
    distance_miles = models.FloatField(default=0.0, blank=False, null=False)
    duration_seconds = models.IntegerField(default=0, blank=False, null=False)
    route = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['started_event__time']

    def __str__(self):
        if self.ended_event is not None:
            formatstr = 'Trip starting with %s and ending with %s during %s'
            values    = (self.started_event, self.ended_event, self.session)
            return formatstr % values
        else:
            formatstr = 'Unfinished trip starting with %s during %s'
            values    = (self.started_event, self.session)
            return formatstr % values

    def compute_distance(self):
        if self.started_event and self.ended_event:
            end_location = self.ended_event.location.transform(
                lat_lon_to_spheroid, clone=True)
            start_location = self.started_event.location.transform(
                lat_lon_to_spheroid, clone=True)
            return D(m=end_location.distance(start_location)).mi
        else:
            return 0.0

    def compute_duration(self):
        if self.started_event and self.ended_event:
            timediff = self.ended_event.time - self.started_event.time
            return timediff.total_seconds()
        else:
            return 0

    def end(self, lock_event):
        if self.ended_event is None:
            self.ended_event = lock_event
            self.distance_miles = self.compute_distance()
            self.duration_seconds = self.compute_duration()
            self.save()
        else:
            raise InvalidTripStateException(message='Trip has already ended.')

    def starting_point(self):
        return self.started_event.location

    def ending_point(self):
        return self.ended_event.location

    def route_entries(self):
        pattern = r'([\d\.-]+),([\d\.-]+),([\d\.-]+),([\d\.-]+),([\dT\:\.-]+)\n'
        regex = re.compile(pattern)
        entries = []
        if (self.route):
            for (bearing, lat, lng, alt, time) in regex.findall(self.route):
                timestamp = parse_datetime(time)
                entries.append((float(bearing), float(lat), float(lng), float(alt), timestamp))
        return entries

    def update_bike_position_from_route(self):
        last_route_entry = self.route.split('\n')[-1]
        pattern = r'([\-\d\.]+),([\-\d\.]+),([\-\d\.]+),([\-\d\.]+),(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2}).(\d{0,6})([\-\+]?\d{4})'
        match_result = re.match(pattern, last_route_entry)
        lat = match_result.group(2)
        lng = match_result.group(3)
        self.session.bike.update_last_location(Point(float(lng), float(lat)))

    def save(self, *args, **kwargs):
        super(Trip, self).save(*args, **kwargs)
        ws_notifications.session_status(self.session)


class FeedbackPrompt(BaasBaseObject):
    default = models.CharField(max_length=255)
    sort_index = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.default


class FeedbackPromptSet(BaasBaseObject):
    name = models.CharField(
        max_length=255, blank=False, null=False, unique=True)
    prompts = models.ManyToManyField(FeedbackPrompt)

    def __str__(self):
        return self.name


class FeedbackEvent(BaasBaseObject):
    bike = models.ForeignKey(Bike, on_delete=models.CASCADE, blank=False, null=False)
    reporter = models.ForeignKey(Person, on_delete=models.CASCADE, blank=False, null=False)
    prompts = models.ForeignKey(FeedbackPromptSet, on_delete=models.CASCADE, blank=True, null=True)
    time = models.DateTimeField(blank=False, null=False, auto_now_add=True)
    star_rating = models.SmallIntegerField(default=-1)
    location = gis_models.PointField(blank=True, null=True, srid=4326)
    bike_rideable = models.BooleanField(default=True, blank=False, null=False)

    def __str__(self):
        formatstr = 'Feedback about %s by %s at %s'
        values    = (self.bike, self.reporter, self.time)
        return formatstr % values


class FeedbackEventDetail(BaasBaseObject):
    event = models.ForeignKey(
        FeedbackEvent, on_delete=models.CASCADE, related_name='detail_set', blank=False, null=False)
    prompt = models.ForeignKey(FeedbackPrompt, on_delete=models.CASCADE, blank=False, null=False)
    value = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return '%s : %s' % (self.prompt, self.value)


class BikeOutsideMarketException(Exception):
    def __init__(self, message):
        self.message = message


class InvalidTripStateException(Exception):
    def __init__(self, message):
        self.message = message


class InvalidSessionStateException(Exception):
    def __init__(self, message):
        self.message = message
