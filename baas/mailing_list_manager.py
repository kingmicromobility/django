import os
import mailchimp3


def add_person_to_list(list_id, email, first_name, last_name, phone,
                       original_market, opt_in=True, update=False):
    mailchimp_api = get_mailchimp_api()
    if not mailchimp_api:
        return

    response = ''
    if email == '':
        return {'status': 'error', 'message': 'Email cannot be blank'}
    try:
        email = email.lower()
        try:
            cur_member = mailchimp_api.lists.members.get(list_id, email)
            if not update and cur_member and cur_member.get('status') != 'archived':
                return {'status': 'success', 'message': 'Subscription already exists'}
        except mailchimp3.mailchimpclient.MailChimpError as e:
            if e.args[0].get('status') == 404:
                cur_member = None
            else:
                raise

        subscribe_status = 'pending' if opt_in else 'subscribed'
        update_status = subscribe_status if cur_member and cur_member.get('status') == 'archived' else None
        mailchimp_api.lists.members.create_or_update(list_id, email, {
            'email_address': email,
            'status_if_new': subscribe_status,
            'status': update_status,
            'merge_fields': {
                'FMANE': first_name,
                'LNAME': last_name,
                'PHONE': phone,
                'ORIG_MKT': original_market}
        })
        response = {'status': 'success'}
    except mailchimp3.mailchimpclient.MailChimpError as e:
        response = {
            'status': 'error',
            'message': str(e)}
    return response


def get_mailchimp_api():
    if os.environ.get('MAILCHIMP_KEY'):
        return mailchimp3.MailChimp(mc_api=os.environ.get('MAILCHIMP_KEY'))
