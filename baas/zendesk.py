from zenpy import Zenpy
from zenpy.lib.api_objects import User, Ticket


creds = {
    'email': 'help@baasbikes.com',
    'token': 'tC4YLa0SxqI3M2FtkLtseIPcjpY6yoz0WYcWomTr',
    'subdomain': 'baasbikes'
}

zenpy = Zenpy(**creds)


def create_ticket(user_data, ticket_data):
    user = User(**user_data)
    created_user = zenpy.users.create_or_update(user)
    ticket_data['requester_id'] = created_user.id
    ticket = Ticket(**ticket_data)
    zenpy.tickets.create(ticket)
