import braintree
from django.utils import timezone
from django.core.exceptions import ValidationError
from django_rq import job
from baas import mailers


def create_or_update_customer(person):
    if person.payment_gateway_id:
        return update_customer(person)
    else:
        return create_customer(person)


def create_customer(person):
    result = braintree.Customer.create({
        "first_name": person.first_name,
        "last_name": person.last_name,
        "email": person.user.email,
        "phone": person.phone_number})
    if result.is_success:
        return result.customer.id
    else:
        raise ValidationError(
            {'non_field_errors': 'Failed payment gateway validation'})


def update_customer(person):
    result = braintree.Customer.update(person.payment_gateway_id, {
        "first_name": person.first_name,
        "last_name": person.last_name,
        "email": person.user.email,
        "phone": person.phone_number})
    if result.is_success:
        return result.customer.id
    else:
        raise ValidationError(
            {'non_field_errors': 'Failed payment gateway validation'})


def list_payment_methods(person):
    if not person.payment_gateway_id:
        person.save()
    result = braintree.Customer.find(person.payment_gateway_id)
    return result.payment_methods


def create_payment_method(person, nonce):
    if not person.payment_gateway_id:
        person.save()
    result = braintree.PaymentMethod.create({
        "customer_id": str(person.payment_gateway_id),
        "payment_method_nonce": nonce,
        "options": {
            "make_default": True}})
    if result.is_success:
        return True
    else:
        raise ValidationError(
            {'non_field_errors': 'Payment gateway refused nonce: %s' % result.message})


@job('default', result_ttl=0)
def create_transaction(person, payment_attempt):
    payment_attempt.attempted_at = timezone.now()
    payment_attempt.amount = payment_attempt.transaction.total_amount
    if person.paying_customer:
        for pm in list_payment_methods(person):
            if pm.default:
                pm_description = '%s ending in %s' % (pm.card_type, pm.last_4)
                payment_attempt.payment_method_description = pm_description
                payment_attempt.payment_method_card_type = pm.card_type
                payment_attempt.payment_method_last_4 = pm.last_4
        payment_attempt.save()
        dollars = str(payment_attempt.transaction.total_amount // 100)
        cents = str(payment_attempt.transaction.total_amount % 100).zfill(2)
        amountstr = '%s.%s' % (dollars, cents)
        result = braintree.Transaction.sale({
            "customer_id": str(person.payment_gateway_id),
            "amount": amountstr,
            "options": {
                "submit_for_settlement": True}})
        if result.is_success:
            payment_attempt.successful = True
            payment_attempt.save()
            payment_attempt.transaction.paid_in_full = True
            payment_attempt.transaction.save()
            mailers.send_session_receipt_email.delay(
                payment_attempt.transaction.session)
            return True
        else:
            raise ValidationError(
                {'non_field_errors': 'Payment gateway refused transaction: %s' % result.message})
    else:
        payment_attempt.payment_method_description = 'Compliments of Baas Inc.'
        payment_attempt.successful = True
        payment_attempt.save()
        payment_attempt.transaction.paid_in_full = True
        payment_attempt.transaction.save()
        mailers.send_session_receipt_email.delay(
            payment_attempt.transaction.session)
        return True
