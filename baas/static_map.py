import polyline


class GoogleStaticMap:

    endpoint = 'https://maps.googleapis.com/maps/api/staticmap'
    width = 512
    height = 512

    def __init__(self, api_key):
        self.api_key = api_key
        self.markers = []
        self.polylines = []

    def set_size(self, width, height):
        self.width = width
        self.height = height

    def add_marker(self, marker):
        self.markers.append(marker)

    def add_polyline(self, polyline):
        self.polylines.append(polyline)

    def collect_params(self):
        params = []
        params.append('size=%sx%s' % (self.width, self.height))
        [params.append(marker.as_url_param()) for marker in self.markers]
        [params.append(polyline.as_url_param()) for polyline in self.polylines]
        params.append('key=%s' % self.api_key)
        return params

    def as_url(self):
        template = '%s?%s'
        params = self.collect_params()
        values = (self.endpoint, '&'.join(params))
        return template % values


class GoogleStaticMapMarker:

    def __init__(self, size, color, label, locations):
        self.size = size
        self.color = color
        self.label = label
        self.locations = locations

    def add_location(self, location):
        self.locations.append(location)

    def as_url_param(self):
        template = 'markers=size:%s|color:%s|label:%s|%s'
        values = (self.size, self.color, self.label, '|'.join(self.locations))
        return template % values


class GoogleStaticMapPolyline:

    def __init__(self, weight, color, locations):
        self.weight = weight
        self.color = color
        self.locations = locations

    def add_location(self, location):
        self.locations.append(location)

    def as_url_param(self):
        template = 'path=weight:%s|color:%s|enc:%s'
        encoded_locations = ''
        if len(self.locations) > 0:
            encoded_locations = polyline.encode(self.locations)
        values = (self.weight, self.color, encoded_locations)
        return template % values
