import factory
from baas import models
from datetime import timedelta
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point
from django.contrib.gis.geos import Polygon
from django.contrib.gis.geos import MultiPolygon


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()
    email = 'fmercury@queen.com'
    password = 'correctpassword'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        manager = cls._get_manager(model_class)
        return manager.create_user(*args, **kwargs)


class SecondUserFactory(UserFactory):
    email = 'donald@trump.com'
    password = 'makeamericagreatagain'


class ThirdUserFactory(UserFactory):
    email = 'justinbieber@baas.com'
    password = 'password'


class FourthUserFactory(UserFactory):
    email = 'gracepotter@baas.com'
    password = 'password'


class PersonFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Person
        exclude = ('is_staff',)

    user = factory.SubFactory(UserFactory, is_staff=factory.SelfAttribute('..is_staff'))
    first_name = 'Freddie'
    last_name = 'Mercury'
    phone_number = '555-867-5309'
    is_staff = False


class SecondPersonFactory(PersonFactory):
    user = factory.SubFactory(SecondUserFactory)
    first_name = 'Donald'
    last_name = 'Trump'
    phone_number = '212-555-5555'


class ThirdPersonFactory(PersonFactory):
    user = factory.SubFactory(ThirdUserFactory)
    first_name = 'Justin'
    last_name = 'Bieber'
    phone_number = '444-444-4444'


class FourthPersonFactory(PersonFactory):
    user = factory.SubFactory(FourthUserFactory)
    first_name = 'Grace'
    last_name = 'Potter'
    phone_number = '444-444-4444'


class MarketFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Market
    name = 'DC Proper Market'
    state = 'active'
    geoshape = MultiPolygon(Polygon((
        Point(-77.1006774902350003, 38.9067077636729977),
        Point(-77.1274566650400004, 38.9376068115239988),
        Point(-77.0375061035170035, 38.9966583251960017),
        Point(-76.9029235839850003, 38.8922882080089991),
        Point(-77.0217132568370033, 38.8030242919929975),
        Point(-77.0237731933600003, 38.8064575195320032),
        Point(-77.0285797119150004, 38.8057708740239988),
        Point(-77.0306396484390064, 38.8064575195320032),
        Point(-77.0306396484390064, 38.8188171386729977),
        Point(-77.0223999023450006, 38.8414764404310020),
        Point(-77.0285797119150004, 38.8572692871100003),
        Point(-77.0443725585950006, 38.8792419433600003),
        Point(-77.0608520507820032, 38.8964080810560020),
        Point(-77.0910644531259948, 38.9019012451179975),
        Point(-77.1013641357429975, 38.9053344726570032),
        Point(-77.1006774902350003, 38.9067077636729977))))


class AttributionChannelOne(factory.django.DjangoModelFactory):
    name = 'Fake Attribution Channel'
    descriptive_phrase = 'Saw a fake attribution channel'
    market = factory.SubFactory(MarketFactory)
    attribution_code = 'fake1234'
    active = True

    class Meta:
        model = models.AttributionChannel


class AttributionChannelTwo(factory.django.DjangoModelFactory):
    name = 'Fake Ambassador Attribution Channel'
    descriptive_phrase = 'Met a fake ambassador'
    market = None
    attribution_code = 'fakeamb123'
    active = False

    class Meta:
        model = models.AttributionChannel


class BikeBrandFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.BikeBrand
    name = 'Trek'


class BikeSizeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.BikeSize
    name = '22-inch'


class BikeStyleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.BikeStyle
    name = 'Mountain'


class BikeImageSetFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.BikeImageSet
    name = 'Blue Priority Classic Image Set'


class BikeImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.BikeImage
    set = factory.SubFactory(BikeImageSetFactory)
    key = 'thumbnail'
    name = 'Blue Priority Classic Thumbnail Image'
    image_uri = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/tAEhQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAADxwBWgADGyVHHAIAAAIAAgA4QklNBCUAAAAAABD84R+JyLfJeC80YjQHWHfr/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFB//Z"


class LockFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Lock
    type = 'seat-post'
    salt = '17'
    baas_lock_id = 'baas-A5:B6:C7:D8:E9:F0'
    battery_remaining = 75.0


class BikeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Bike
    name = 'Test Bike'
    status = 'available'
    last_location = Point(-77.027263837, 38.909784928)
    launch_date = timezone.now()
    image_set = factory.SubFactory(BikeImageSetFactory)
    market = factory.SubFactory(MarketFactory)
    brand = factory.SubFactory(BikeBrandFactory)
    size = factory.SubFactory(BikeSizeFactory)
    style = factory.SubFactory(BikeStyleFactory)
    lock = factory.SubFactory(LockFactory)


class BikeDetailFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.BikeDetail
    bike = factory.SubFactory(BikeFactory)
    name = 'Fenders'
    value = 'True'


class BikeDetailPriceWithLockFactory(BikeDetailFactory):
    name = 'price_with_lock'
    value = '200'


class ReservationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Reservation
    renter = factory.SubFactory(PersonFactory)
    bike   = factory.SubFactory(BikeFactory)
    market = factory.SubFactory(MarketFactory)


class CanceledReservationFactory(ReservationFactory):
    canceled_at = timezone.now()


class ExpiredReservationFactory(ReservationFactory):
    expires_at = timezone.now() - timedelta(minutes=20)


class AbstractLockEventFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.LockEvent
        abstract = True
    bike = factory.SubFactory(BikeFactory)
    person = factory.SubFactory(PersonFactory)
    location = Point(-77.027263837, 38.909784928)


class UnlockEventFactory(AbstractLockEventFactory):
    event_type = 'unlocked'


class LockEventFactory(AbstractLockEventFactory):
    event_type = 'locked'


class SessionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Session
    reservation = factory.SubFactory(ReservationFactory)
    renter = factory.LazyAttribute(lambda obj: obj.reservation.renter)
    bike = factory.LazyAttribute(lambda obj: obj.reservation.bike)
    market = factory.SubFactory(MarketFactory)
    started_event = factory.LazyAttribute(
        lambda obj: UnlockEventFactory.build(bike=obj.bike, person=obj.renter))
    started_at = timezone.now()


class EndedSessionFactory(SessionFactory):
    ended_at = timezone.now()


class FeedbackPromptFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.FeedbackPrompt
    default = 'Do the brakes still work?'


class FeedbackPromptSetFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.FeedbackPromptSet
    name = 'post_session_feedback'


class FeedbackEventFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.FeedbackEvent
    bike = factory.SubFactory(BikeFactory)
    reporter = factory.SubFactory(PersonFactory)
    prompts = factory.SubFactory(FeedbackPromptSetFactory)
    time = timezone.now()
    star_rating = 4
    location = Point(-77.027263837, 38.909784928)


class FeedbackEventDetailFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.FeedbackEventDetail
    event = factory.SubFactory(FeedbackEventFactory)
    prompt = factory.SubFactory(FeedbackPromptFactory)
    value = 'No.'


def create_feedback_prompt_sets():
    """
    init feedback prompt sets used in mobile app
    """
    prompt_set_names = [
        'out_of_session_problem',
        'in_session_problem',
        'session_end_feedback',
    ]
    default_feedback_prompt = None
    for prompt_set_name in prompt_set_names:
        prompt_set = FeedbackPromptSetFactory(name=prompt_set_name)
        # add default prompt to content
        default_feedback_prompt = default_feedback_prompt or FeedbackPromptFactory()
        prompt_set.prompts.add(default_feedback_prompt)
