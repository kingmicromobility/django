import braintree
import operator
from functools import reduce
from django.utils import timezone
from django.contrib.auth import models as auth_models
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import fromstr
from django.contrib.gis.geos.point import Point
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance
from django.db.models import Q, Prefetch
from django.core.exceptions import ValidationError
from rest_framework import viewsets, mixins, permissions, status, parsers, exceptions
from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle
from baas import models, serializers, payment_gateway, mailing_list_manager
from baas import permissions as baas_permissions
from baas import zendesk

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger('baas_debug')


class UserViewSet(viewsets.ModelViewSet):
    user_model       = get_user_model()
    queryset         = user_model.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.IsAdminUser,)


class GroupViewSet(viewsets.ModelViewSet):
    queryset         = auth_models.Group.objects.all()
    serializer_class = serializers.GroupSerializer
    permission_classes = (baas_permissions.IsAdminUserOrIsAuthenticatedAndReadOnly,)


class PersonViewSet(viewsets.ModelViewSet):
    permission_classes = (baas_permissions.IsAuthenticatedOrCreateOnly,)
    serializer_class = serializers.PersonSerializer

    def get_queryset(self):
        if self.request.user and self.request.user.is_staff:
            return models.Person.objects.all().order_by('-created_at')
        else:
            return models.Person.objects.filter(user=self.request.user)


class MarketViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.MarketSerializer
    permission_classes = (baas_permissions.IsAdminUserOrReadOnly,)

    def get_queryset(self):
        lat = self.request.query_params.get('lat', None)
        lon = self.request.query_params.get('lon', None)
        if lat and lon:
            lat_lon_str    = 'POINT(%s %s)' % (lon, lat)
            lat_lon        = fromstr(lat_lon_str, srid=4326)
            return models.Market.objects\
                .filter(state='active')\
                .annotate(distance=Distance('geoshape', lat_lon))\
                .prefetch_related('bike_set')\
                .prefetch_related('bike_set__lock')\
                .prefetch_related('bike_set__size')\
                .prefetch_related('bike_set__style')\
                .prefetch_related('bike_set__brand')\
                .prefetch_related('bike_set__detail_set')\
                .order_by('distance')[:1]
        else:
            return models.Market.objects\
                .filter(state='active')\
                .prefetch_related('bike_set')\
                .prefetch_related('bike_set__lock')\
                .prefetch_related('bike_set__size')\
                .prefetch_related('bike_set__style')\
                .prefetch_related('bike_set__brand')\
                .prefetch_related('bike_set__detail_set')\
                .order_by('-created_at')


class MarketContentsViewSet(mixins.ListModelMixin, viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request, market_pk=None):
        try:
            market = models.Market.objects.get(pk=market_pk)
        except models.Market.DoesNotExist as e:
            raise exceptions.NotFound(e)
        lat    = float(self.request.query_params.get('lat', None))
        lon    = float(self.request.query_params.get('lon', None))
        if lat and lon:
            within_market = market.contains(Point(lon, lat))
            serializer = serializers.MarketContentsSerializer({
                'within_market': within_market})
            return Response(serializer.data)
        else:
            return Response(
                {'non_field_errors': 'both "lat" and "lon" params required'},
                status=status.HTTP_422_UNPROCESSABLE)


class MarketSessionsViewSet(mixins.ListModelMixin, viewsets.ViewSet):
    serializer_class = serializers.SessionSerializer

    def list(self, request, market_pk=None):
        try:
            market = models.Market.objects.get(pk=market_pk)
        except models.Market.DoesNotExist as e:
            raise exceptions.NotFound(e)
        status = self.request.query_params.get('status', None)
        query = [Q(bike__market=market)]

        if not self.request.user.is_staff:
            query.append(Q(renter__user=request.user))

        if status == 'live':
            query.append(Q(ended_at__isnull=True))
        elif status == 'ended':
            query.append(Q(ended_at__isnull=False))
        sessions = models.Session.objects.filter(
            reduce(operator.and_, query)).order_by('created_at')
        return Response(self.serializer_class(sessions, many=True).data)


class BikeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.BikeSerializer
    permission_classes = (baas_permissions.IsAdminUserOrReadOnly,)
    parser_classes = (parsers.JSONParser, parsers.MultiPartParser)

    def get_queryset(self):
        lat    = self.request.query_params.get('last_lat', None)
        lon    = self.request.query_params.get('last_lon', None)
        radius = self.request.query_params.get('radius', 10)
        status = self.request.query_params.get('status', None)
        query  = []
        if lat and lon:
            lat_lon_str = 'POINT(%s %s)' % (lon, lat)
            lat_lon     = fromstr(lat_lon_str, srid=4326)
            query.append(
                Q(last_location__distance_lte=(lat_lon, D(mi=radius))))
        if status:
            query.append(Q(status=status))
        if len(query) > 0:
            return models.Bike.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.Bike.objects.all().order_by('-created_at')


class BikeBrandViewSet(viewsets.ModelViewSet):
    queryset         = models.BikeBrand.objects.all().order_by('-created_at')
    serializer_class = serializers.BikeBrandSerializer
    permission_classes = (baas_permissions.IsAdminUserOrReadOnly,)


class BikeDetailViewSet(viewsets.ModelViewSet):
    queryset         = models.BikeDetail.objects.all().order_by('-created_at')
    serializer_class = serializers.BikeDetailSerializer
    permission_classes = (baas_permissions.IsAdminUserOrReadOnly,)


class BikeSizeViewSet(viewsets.ModelViewSet):
    queryset         = models.BikeSize.objects.all().order_by('-created_at')
    serializer_class = serializers.BikeSizeSerializer
    permission_classes = (baas_permissions.IsAdminUserOrReadOnly,)


class BikeStyleViewSet(viewsets.ModelViewSet):
    queryset         = models.BikeStyle.objects.all().order_by('-created_at')
    serializer_class = serializers.BikeStyleSerializer
    permission_classes = (baas_permissions.IsAdminUserOrReadOnly,)


class LockEventUserRateThrottle(UserRateThrottle):
    scope = 'lock_event'
    rate = '1/s'
    cache_format = 'throttle_%(scope)s_%(ident)s_%(event_type)s'

    def get_cache_key(self, request, view):
        if not request.user.is_authenticated:
            return None
        ident = request.user.pk
        event_type = request.query_params.get('event_type')

        return self.cache_format % {
            'scope': self.scope,
            'ident': ident,
            'event_type': event_type,
        }


class LockEventViewSet(viewsets.ModelViewSet):
    queryset         = models.LockEvent.objects.all().order_by('-created_at')
    serializer_class = serializers.LockEventSerializer
    paginate_by = 100
    paginate_by_param = 'page_size'
    max_paginate_by = 1000

    def get_throttles(self):
        if self.action == 'create':
            return [LockEventUserRateThrottle(), ]
        else:
            return super().get_throttles()


class ReservationViewSet(viewsets.ModelViewSet):
    query = []
    serializer_class = serializers.ReservationSerializer

    def generate_query(self, query):
        status = self.request.query_params.get('status')
        now = timezone.now()
        if status and status == 'active':
            query.append(Q(
                started_at__lte=now, expires_at__gte=now, canceled_at=None,
                session_started_at=None))
        if status and status == 'expired':
            query.append(Q(
                expires_at__lte=now, canceled_at=None,
                session_started_at=None))
        if status and status == 'canceled':
            query.append(Q(canceled_at__lte=now, session_started_at=None))
        return query

    def get_queryset(self):
        if self.request.user.is_staff:
            query = self.generate_query([])
        else:
            query = self.generate_query([Q(renter__user=self.request.user)])
        if len(query) > 0:
            return models.Reservation.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.Reservation.objects.all().order_by(
                '-created_at')

    def create(self, request):
        person = models.Person.objects.get(user=self.request.user)
        methods = payment_gateway.list_payment_methods(person)
        if len(methods) > 0:
            return super(ReservationViewSet, self).create(request)
        else:
            return Response(
                {'non_field_errors': 'User has no payment methods.'},
                status=status.HTTP_402_PAYMENT_REQUIRED)


class PersonReservationViewSet(ReservationViewSet):
    serializer_class = serializers.ReservationSerializer

    def get_queryset(self):
        person = models.Person.objects.get(user=self.request.user)
        query = super(PersonReservationViewSet, self).generate_query(
            [Q(renter=person)])
        if len(query) > 0:
            return models.Reservation.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.Reservation.objects.all().order_by('-created_at')


class SessionUserRateThrottle(UserRateThrottle):
    scope = 'session'
    rate = '1/s'


class SessionViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.SessionSerializer

    def generate_query(self, query):
        status = self.request.query_params.get('status')
        now = timezone.now()
        if status and status == 'active':
            query.append(Q(ended_at=None))
        if status and status == 'ended':
            query.append(Q(ended_at__lte=now))
        return query

    def get_queryset(self):
        if self.request.user.is_staff:
            query = self.generate_query([])
        else:
            query = self.generate_query([Q(renter__user=self.request.user)])
        if len(query) > 0:
            return models.Session.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.Session.objects.all().order_by('-created_at')

    def get_throttles(self):
        if self.action == 'update':
            return [SessionUserRateThrottle(), ]
        else:
            return super().get_throttles()


class PersonSessionViewSet(SessionViewSet):
    serializer_class = serializers.SessionSerializer

    def get_queryset(self):
        person = models.Person.objects.get(user=self.request.user)
        query = super(PersonSessionViewSet, self).generate_query(
            [Q(renter=person)])
        if len(query) > 0:
            return models.Session.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.Session.objects.all().order_by('-created_at')


class BikeSessionViewSet(viewsets.ViewSet):
    serializer_class = serializers.SessionSerializer

    def list(self, request, bike_pk):
        try:
            bike = models.Bike.objects.get(pk=bike_pk)
        except models.Bike.DoesNotExist as e:
            raise exceptions.NotFound(e)
        queryset = models.Session.objects.filter(
            bike=bike).order_by('-created_at')
        if not request.user.is_staff:
            queryset = queryset.filter(renter__user=request.user)
        return Response(self.serializer_class(queryset, many=True).data)


class TripSoldUserRateThrottle(UserRateThrottle):
    scope = 'trip'
    rate = '1/s'

    def allow_request(self, request, view):
        if request.query_params.get('sold'):
            return super().allow_request(request, view)
        else:
            return True


class TripViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.TripSerializer

    def generate_query(self, query):
        status = self.request.query_params.get('status')
        now = timezone.now()
        if status and status == 'active':
            query.append(Q(ended_event=None))
        if status and status == 'ended':
            query.append(Q(ended_event__time__lte=now))
        return query

    def get_queryset(self):
        if self.request.user.is_staff:
            query = self.generate_query([])
        else:
            query = self.generate_query([Q(session__renter__user=self.request.user)])
        if len(query) > 0:
            return models.Trip.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.Trip.objects.all().order_by('-created_at')

    def get_throttles(self):
        if self.action == 'update':
            return [TripSoldUserRateThrottle(), ]
        else:
            return super().get_throttles()


class PersonTripViewSet(TripViewSet):
    serializer_class = serializers.TripSerializer

    def get_queryset(self):
        person = models.Person.objects.get(user=self.request.user)
        query = super(PersonTripViewSet, self).generate_query(
            [Q(session__renter=person)])
        if len(query) > 0:
            return models.Trip.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.Trip.objects.all().order_by('-created_at')


class BikeTripViewSet(viewsets.ViewSet):
    serializer_class = serializers.TripSerializer

    def list(self, request, bike_pk):
        try:
            bike = models.Bike.objects.get(pk=bike_pk)
        except models.Person.DoesNotExist as e:
            raise exceptions.NotFound(e)
        queryset = models.Trip.objects.filter(
            session__bike=bike).order_by('-created_at')
        if not request.user.is_staff:
            queryset = queryset.filter(session__renter__user=request.user)
        return Response(self.serializer_class(queryset, many=True).data)


class FeedbackEventViewSet(viewsets.ModelViewSet):
    queryset = models.FeedbackEvent.objects.all().order_by('-created_at')
    serializer_class = serializers.FeedbackEventSerializer


class FeedbackPromptSetViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = models.FeedbackPromptSet.objects.all().order_by('-created_at')
    serializer_class = serializers.FeedbackPromptSetSerializer

    def get_queryset(self):
        name = self.request.query_params.get('name', None)
        query = []
        if name:
            query.append(Q(name=name))
        if len(query) > 0:
            return models.FeedbackPromptSet.objects.filter(
                reduce(operator.and_, query)).order_by('-created_at')
        else:
            return models.FeedbackPromptSet.objects.all().order_by(
                '-created_at')


class PaymentViewSet(viewsets.ViewSet):
    authentication_classes = ()
    permission_classes = ()

    def list(self, request):
        return Response({'token': braintree.ClientToken.generate()})


class PaymentMethodViewSet(mixins.ListModelMixin, viewsets.ViewSet):
    def create(self, request, person_pk=None):
        try:
            requested_person = models.Person.objects.get(pk=person_pk)
            logged_in_person = models.Person.objects.get(user=request.user)
        except models.Person.DoesNotExist as e:
            raise exceptions.NotFound(e)
        if requested_person == logged_in_person:
            try:
                payment_gateway.create_payment_method(
                    logged_in_person, request.data['nonce'])
            except ValidationError as e:
                logger.error(e)
                return Response(e.message_dict, status=status.HTTP_400_BAD_REQUEST)
            return Response({}, status=status.HTTP_201_CREATED)
        else:
            return Response({}, status=status.HTTP_403_FORBIDDEN)

    def list(self, request, person_pk=None):
        try:
            person = models.Person.objects.get(pk=person_pk)
        except models.Person.DoesNotExist as e:
            raise exceptions.NotFound(e)
        if person.user == request.user:
            methods = payment_gateway.list_payment_methods(person)
            serializer = serializers.PaymentMethodSerializer(methods, many=True)
            return Response(serializer.data)
        else:
            return Response({}, status=status.HTTP_403_FORBIDDEN)


class TemporaryPasswordViewSet(viewsets.ViewSet):
    authentication_classes = ()
    permission_classes = ()

    def create(self, request):
        try:
            person = models.Person.objects.get(
                user__email=request.data['email'])
            person.reset_password()
        except models.Person.DoesNotExist:
            pass
        # We always want to return a 201. Otherwise we'll be indicating which
        # emails are valid accounts.
        return Response({}, status=status.HTTP_201_CREATED)


class AnswerCodesViewSet(viewsets.ViewSet):
    def create(self, request):
        logged_in_person = models.Person.objects.get(user=request.user)
        lock = models.Lock.find_by_urn(request.data.get('lock'))
        challenge_code = request.data.get('challenge_code')
        answer_code = lock.answer_code(logged_in_person, challenge_code)
        if answer_code >= 0:
            serializer = serializers.AnswerCodeSerializer(
                {'answer_code': answer_code})
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                {'non_field_errors': 'Invalid bike for user.'},
                status=status.HTTP_403_FORBIDDEN)


class SubscriberViewSet(viewsets.ViewSet):
    authentication_classes = ()
    permission_classes = ()

    def create(self, request):
        result = mailing_list_manager.add_person_to_list(
            request.data.get('list_id'),
            request.data.get('email'),
            request.data.get('first_name', None),
            request.data.get('last_name', None),
            request.data.get('phone', None),
            request.data.get('original_market', None),
            update=True)
        if result['status'] == 'error':
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(result, status=status.HTTP_201_CREATED)


class ZendeskTicketViewSet(viewsets.ViewSet):
    authentication_classes = ()
    permission_classes = ()

    def create(self, request):
        user_data = {
            'name': request.data.get('name', 'Anonymous'),
            'email': request.data.get('email', None),
            'phone': request.data.get('phone', None)
        }
        ticket_data = {
            'subject': request.data.get('subject', 'No Subject'),
            'description': request.data.get('description', None)
        }
        ticket = zendesk.create_ticket(user_data, ticket_data)
        return Response(ticket, status=status.HTTP_201_CREATED)
