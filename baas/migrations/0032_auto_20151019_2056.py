# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0031_auto_20151019_2046'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bike',
            name='ambassador',
            field=models.ForeignKey(related_name='bike_ambassador_set', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='bike',
            name='owner',
            field=models.ForeignKey(related_name='bike_owned_set', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
        ),
    ]
