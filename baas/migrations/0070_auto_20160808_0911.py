# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-08-08 09:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0069_auto_20160808_0857'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='financialtransactionlineitem',
            name='seq',
        ),
    ]
