# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0043_lockevent_allow_outside_market'),
    ]

    operations = [
        migrations.RenameField(
            model_name='lockevent',
            old_name='allow_outside_market',
            new_name='outside_market',
        ),
    ]
