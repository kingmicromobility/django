# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0015_auto_20151013_2109'),
    ]

    operations = [
        migrations.CreateModel(
            name='LockEvent',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('event_type', models.CharField(max_length=255)),
                ('time', models.DateTimeField()),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
                ('person', models.ForeignKey(to='baas.Person', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Trip',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('ended_event', models.ForeignKey(related_name='session_ended_event', blank=True, to='baas.LockEvent', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='bikelocation',
            name='bike',
        ),
        migrations.RemoveField(
            model_name='bikelocation',
            name='person',
        ),
        migrations.RemoveField(
            model_name='bikelockaction',
            name='person',
        ),
        migrations.RemoveField(
            model_name='bikelockaction',
            name='session',
        ),
        migrations.RemoveField(
            model_name='userbikerole',
            name='bike',
        ),
        migrations.RemoveField(
            model_name='userbikerole',
            name='person',
        ),
        migrations.RenameField(
            model_name='reservation',
            old_name='person',
            new_name='renter',
        ),
        migrations.AddField(
            model_name='reservation',
            name='canceled_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reservation',
            name='expires_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 13, 22, 23, 28, 87886, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reservation',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reservation',
            name='started_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 13, 22, 23, 41, 560968, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='session',
            name='ambassador',
            field=models.ForeignKey(related_name='session_bike_ambassador', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='session',
            name='ended_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 13, 22, 23, 51, 818365, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='session',
            name='owner',
            field=models.ForeignKey(related_name='session_bike_owner', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='session',
            name='renter',
            field=models.ForeignKey(related_name='session_bike_renter', default=1, to='baas.Person', on_delete=models.CASCADE),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='session',
            name='reservation',
            field=models.ForeignKey(default=1, to='baas.Reservation', on_delete=models.CASCADE),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='session',
            name='started_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 10, 13, 22, 24, 26, 44916, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='BikeLocation',
        ),
        migrations.DeleteModel(
            name='BikeLockAction',
        ),
        migrations.DeleteModel(
            name='UserBikeRole',
        ),
        migrations.AddField(
            model_name='trip',
            name='session',
            field=models.ForeignKey(to='baas.Session', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='trip',
            name='started_event',
            field=models.ForeignKey(related_name='session_started_event', to='baas.LockEvent', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='session',
            name='ended_event',
            field=models.ForeignKey(related_name='session_ended_lock_event', blank=True, to='baas.LockEvent', on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='session',
            name='started_event',
            field=models.ForeignKey(related_name='session_started_lock_event', default=1, to='baas.LockEvent', on_delete=models.CASCADE),
            preserve_default=False,
        ),
    ]
