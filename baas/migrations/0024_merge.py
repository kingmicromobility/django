# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0021_merge'),
        ('baas', '0023_auto_20151015_1807'),
    ]

    operations = [
    ]
