# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0006_auto_20151005_2231'),
    ]

    operations = [
        migrations.AddField(
            model_name='bike',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bike',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bike',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelocation',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelocation',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelocation',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelockaction',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelockaction',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelockaction',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikerelationship',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikerelationship',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikerelationship',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='market',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='market',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='market',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='reservation',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='reservation',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='reservation',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='createdAt',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='lastModified',
            field=models.DateTimeField(db_index=True, auto_now=True, null=True),
            preserve_default=True,
        ),
    ]
