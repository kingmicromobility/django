# -*- coding: utf-8 -*-
# Generated by Django 1.9b1 on 2015-12-06 23:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0052_auto_20151206_2307'),
    ]

    operations = [
        migrations.AddField(
            model_name='lockevent',
            name='lock',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='baas.Lock'),
        ),
    ]
