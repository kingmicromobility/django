# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-08-25 09:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0070_auto_20160808_0911'),
    ]

    operations = [
        migrations.AddField(
            model_name='market',
            name='state',
            field=models.CharField(choices=[(b'pre-launch', b'pre-launch'), (b'active', b'active'), (b'closed', b'closed')], default=b'pre-launch', max_length=255),
        ),
    ]
