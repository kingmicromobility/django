# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0016_auto_20151013_2224'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reservation',
            name='bike',
        ),
        migrations.RemoveField(
            model_name='reservation',
            name='renter',
        ),
        migrations.RemoveField(
            model_name='session',
            name='ambassador',
        ),
        migrations.RemoveField(
            model_name='session',
            name='bike',
        ),
        migrations.RemoveField(
            model_name='session',
            name='ended_event',
        ),
        migrations.RemoveField(
            model_name='session',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='session',
            name='renter',
        ),
        migrations.RemoveField(
            model_name='session',
            name='reservation',
        ),
        migrations.RemoveField(
            model_name='session',
            name='started_event',
        ),
        migrations.RemoveField(
            model_name='trip',
            name='session',
        ),
        migrations.DeleteModel(
            name='Reservation',
        ),
        migrations.DeleteModel(
            name='Session',
        ),
    ]
