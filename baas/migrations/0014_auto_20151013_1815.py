# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0013_auto_20151013_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bike',
            name='brand',
            field=models.ForeignKey(related_name='bike_brand', blank=True, to='baas.BikeBrand', on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='bike',
            name='size',
            field=models.ForeignKey(related_name='bike_size', blank=True, to='baas.BikeSize', on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='bike',
            name='style',
            field=models.ForeignKey(related_name='bike_style', blank=True, to='baas.BikeStyle', on_delete=models.CASCADE, null=True),
        ),
    ]
