# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0045_session_end_outside_market'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='session_started_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
