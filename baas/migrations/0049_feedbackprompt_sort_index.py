# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0048_auto_20151027_2219'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedbackprompt',
            name='sort_index',
            field=models.SmallIntegerField(default=0),
        ),
    ]
