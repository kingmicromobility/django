# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0034_auto_20151019_2157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackevent',
            name='time',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
