# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0011_auto_20151012_2031'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bike',
            old_name='ambassador_id',
            new_name='ambassador',
        ),
        migrations.RenameField(
            model_name='bike',
            old_name='owner_id',
            new_name='owner',
        ),
    ]
