# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-07-23 04:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0063_auto_20160722_1954'),
    ]

    operations = [
        migrations.AddField(
            model_name='market',
            name='bike_image_sets',
            field=models.ManyToManyField(through='baas.Bike', to='baas.BikeImageSet'),
        ),
    ]
