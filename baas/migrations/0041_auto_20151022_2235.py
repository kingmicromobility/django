# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0040_auto_20151020_2241'),
    ]

    operations = [
        migrations.CreateModel(
            name='Market',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('geoshape', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='bike',
            name='market',
            field=models.ForeignKey(blank=True, to='baas.Market', on_delete=models.CASCADE, null=True),
        ),
    ]
