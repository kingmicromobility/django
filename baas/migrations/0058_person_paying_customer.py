# -*- coding: utf-8 -*-
# Generated by Django 1.9b1 on 2016-01-03 04:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0057_auto_20151221_1713'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='paying_customer',
            field=models.BooleanField(default=True),
        ),
    ]
