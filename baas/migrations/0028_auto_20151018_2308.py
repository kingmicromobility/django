# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0027_auto_20151016_1846'),
    ]

    operations = [
        # migrations.AlterField(
        #     model_name='bike',
        #     name='ambassador',
        #     field=models.ForeignKey(related_name='ambassador_bikes_set', blank=True, to='baas.Person', null=True),
        # ),
        # migrations.AlterField(
        #     model_name='bike',
        #     name='current_renter',
        #     field=models.ForeignKey(related_name='currently_rented_bikes_set', blank=True, to='baas.Person', null=True),
        # ),
        # migrations.AlterField(
        #     model_name='bike',
        #     name='owner',
        #     field=models.ForeignKey(related_name='owned_bikes_set', blank=True, to='baas.Person', null=True),
        # ),
    ]
