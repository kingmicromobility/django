# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0038_person_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='braintree_id',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='session',
            name='total_price',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
