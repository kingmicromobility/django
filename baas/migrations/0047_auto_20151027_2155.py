# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0046_reservation_session_started_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackevent',
            name='star_rating',
            field=models.SmallIntegerField(),
        ),
    ]
