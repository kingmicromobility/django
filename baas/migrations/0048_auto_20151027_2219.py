# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0047_auto_20151027_2155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackevent',
            name='star_rating',
            field=models.SmallIntegerField(default=-1),
        ),
    ]
