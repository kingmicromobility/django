# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0008_bike_last_location'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserBikeRole',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('is_owner', models.BooleanField(default=False)),
                ('is_ambassador', models.BooleanField(default=False)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
                ('person', models.ForeignKey(to='baas.Person', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='bikerelationship',
            name='bike',
        ),
        migrations.RemoveField(
            model_name='bikerelationship',
            name='person',
        ),
        migrations.DeleteModel(
            name='BikeRelationship',
        ),
        migrations.AddField(
            model_name='person',
            name='name',
            field=models.CharField(default='noname', max_length=255),
            preserve_default=False,
        ),
    ]
