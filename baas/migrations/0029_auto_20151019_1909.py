# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0028_auto_20151018_2308'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeedbackEvent',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('time', models.DateTimeField()),
                ('star_rating', models.PositiveSmallIntegerField()),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FeedbackEventDetail',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('value', models.CharField(max_length=255, null=True, blank=True)),
                ('event', models.ForeignKey(to='baas.FeedbackEvent', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FeedbackPrompt',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('english', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FeedbackPromptSet',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('prompts', models.ManyToManyField(to='baas.FeedbackPrompt')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='feedbackeventdetail',
            name='prompt',
            field=models.ForeignKey(to='baas.FeedbackPrompt', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='feedbackevent',
            name='prompts',
            field=models.ForeignKey(blank=True, to='baas.FeedbackPromptSet', on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='feedbackevent',
            name='reporter',
            field=models.ForeignKey(to='baas.Person', on_delete=models.CASCADE),
        ),
    ]
