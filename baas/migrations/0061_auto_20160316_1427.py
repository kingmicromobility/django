# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-16 14:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0060_auto_20160315_1309'),
    ]

    operations = [
        migrations.RenameField(
            model_name='attributionchannel',
            old_name='channel_code',
            new_name='attribution_code',
        ),
        migrations.RenameField(
            model_name='attributionchannel',
            old_name='app_store_url',
            new_name='branch_url',
        ),
        migrations.RemoveField(
            model_name='attributionchannel',
            name='play_store_url',
        ),
        migrations.AlterField(
            model_name='attributionchannel',
            name='auto_reported_people',
            field=models.ManyToManyField(blank=True, null=True, related_name='attribution_auto_reported', to='baas.Person'),
        ),
        migrations.AlterField(
            model_name='attributionchannel',
            name='self_reported_people',
            field=models.ManyToManyField(blank=True, null=True, related_name='attribution_self_reported', to='baas.Person'),
        ),
    ]
