# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('baas', '0004_market'),
    ]

    operations = [
        migrations.CreateModel(
            name='BikeLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BikeLockAction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_locking', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BikeRelationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_owner', models.BooleanField(default=False)),
                ('is_ambassador', models.BooleanField(default=False)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
                ('person', models.ForeignKey(to='baas.Person', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='bikerelationship',
            name='person',
            field=models.ForeignKey(to='baas.Person', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelockaction',
            name='person',
            field=models.ForeignKey(blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelockaction',
            name='session',
            field=models.ForeignKey(blank=True, to='baas.Session',on_delete=models.CASCADE, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelocation',
            name='person',
            field=models.ForeignKey(blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='bike',
            name='user',
        ),
        migrations.AddField(
            model_name='bike',
            name='current_renter',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bike',
            name='status',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
