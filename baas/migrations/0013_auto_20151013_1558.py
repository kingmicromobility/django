# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0012_auto_20151012_2052'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bikedetail',
            old_name='bike_id',
            new_name='bike',
        ),
        migrations.AddField(
            model_name='bike',
            name='brand',
            field=models.ForeignKey(blank=True, to='baas.BikeBrand', on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='bike',
            name='size',
            field=models.ForeignKey(blank=True, to='baas.BikeSize', on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='bike',
            name='style',
            field=models.ForeignKey(blank=True, to='baas.BikeStyle', on_delete=models.CASCADE, null=True),
        ),
    ]
