# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0026_auto_20151015_2240'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bike',
            old_name='owner_bike_status',
            new_name='owner_available',
        ),
        migrations.RemoveField(
            model_name='bike',
            name='available',
        ),
        migrations.RemoveField(
            model_name='bike',
            name='baas_bike_status',
        ),
        migrations.AddField(
            model_name='bike',
            name='baas_available',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='bike',
            name='status',
            field=models.CharField(default=b'out_of_fleet', max_length=255, choices=[(b'available', b'available'), (b'reserved', b'reserved'), (b'under_maintenance', b'under_maintenance'), (b'out_of_fleet', b'out_of_fleet')]),
        ),
    ]
