# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0032_auto_20151019_2056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bike',
            name='current_renter',
            field=models.ForeignKey(related_name='currently_rented_bikes_set', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
        ),
    ]
