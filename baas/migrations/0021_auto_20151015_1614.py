# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0020_auto_20151014_0620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lockevent',
            name='event_type',
            field=models.CharField(max_length=25, choices=[(b'lock', b'lock'), (b'unlock', b'unlock')]),
        ),
        migrations.AlterField(
            model_name='lockevent',
            name='time',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='ambassador',
            field=models.ForeignKey(related_name='session_bike_ambassador_set', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='owner',
            field=models.ForeignKey(related_name='session_bike_owner_set', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='renter',
            field=models.ForeignKey(related_name='session_set', to='baas.Person', on_delete=models.CASCADE),
        ),
    ]
