# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0024_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bike',
            name='current_renter',
            field=models.ForeignKey(related_name='currently_rented_bikes_set', blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
        ),
    ]
