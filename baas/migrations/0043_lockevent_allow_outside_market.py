# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0042_feedbackevent_bike_rideable'),
    ]

    operations = [
        migrations.AddField(
            model_name='lockevent',
            name='allow_outside_market',
            field=models.BooleanField(default=False),
        ),
    ]
