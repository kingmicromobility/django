# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0041_auto_20151022_2235'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedbackevent',
            name='bike_rideable',
            field=models.BooleanField(default=True),
        ),
    ]
