# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-07-22 19:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0062_auto_20160316_1446'),
    ]

    operations = [
        migrations.CreateModel(
            name='BikeImage',
            fields=[
                ('guid', models.CharField(blank=True, db_index=True, max_length=40, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(auto_now=True, db_index=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('key', models.CharField(choices=[(b'thumbnail', b'thumbnail')], default=b'thumbnail', max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('image_uri', models.TextField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BikeImageSet',
            fields=[
                ('guid', models.CharField(blank=True, db_index=True, max_length=40, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(auto_now=True, db_index=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='bike',
            options={'ordering': ('name',)},
        ),
        migrations.AlterModelOptions(
            name='person',
            options={'ordering': ('first_name', 'last_name')},
        ),
        migrations.AddField(
            model_name='bikeimage',
            name='set',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='baas.BikeImageSet'),
        ),
        migrations.AddField(
            model_name='bike',
            name='image_set',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='baas.BikeImageSet'),
        ),
    ]
