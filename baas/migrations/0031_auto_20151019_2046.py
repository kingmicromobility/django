# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0030_auto_20151019_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackpromptset',
            name='name',
            field=models.CharField(unique=True, max_length=255),
        ),
    ]
