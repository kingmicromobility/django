# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('baas', '0010_auto_20151009_1843'),
    ]

    operations = [
        migrations.CreateModel(
            name='Authority',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BikeBrand',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BikeDetail',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('value', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BikeSize',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BikeStyle',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.DeleteModel(
            name='Market',
        ),
        migrations.RemoveField(
            model_name='bike',
            name='status',
        ),
        migrations.AddField(
            model_name='bike',
            name='ambassador_id',
            field=models.ForeignKey(related_name='bike_ambassador', blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='bike',
            name='available',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bike',
            name='baas_bike_status',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='bike',
            name='launch_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bike',
            name='owner_bike_status',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='bike',
            name='owner_id',
            field=models.ForeignKey(related_name='bike_owner', blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='bike',
            name='current_renter',
            field=models.ForeignKey(related_name='bike_renter', blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
        ),
        migrations.AddField(
            model_name='bikedetail',
            name='bike_id',
            field=models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE),
        ),
    ]
