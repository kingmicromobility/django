# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0022_auto_20151015_1617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lockevent',
            name='event_type',
            field=models.CharField(max_length=25, choices=[(b'locked', b'locked'), (b'unlocked', b'unlocked')]),
        ),
        migrations.AlterField(
            model_name='lockevent',
            name='time',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='session',
            name='ended_event',
            field=models.OneToOneField(related_name='session_ended_lock_event', null=True, blank=True, to='baas.LockEvent', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='session',
            name='started_event',
            field=models.OneToOneField(related_name='session_started_lock_event', to='baas.LockEvent', on_delete=models.CASCADE),
        ),
    ]
