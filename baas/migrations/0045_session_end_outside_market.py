# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0044_auto_20151023_1643'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='end_outside_market',
            field=models.BooleanField(default=False),
        ),
    ]
