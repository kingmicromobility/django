# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0039_auto_20151020_2022'),
    ]

    operations = [
        migrations.RenameField(
            model_name='person',
            old_name='braintree_id',
            new_name='payment_gateway_id',
        ),
    ]
