# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0021_auto_20151015_1614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bike',
            name='ambassador',
            field=models.ForeignKey(related_name='ambassador_bikes_set', blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='bike',
            name='current_renter',
            field=models.ForeignKey(related_name='currently_rented_bikes_set', blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
        ),
        migrations.AlterField(
            model_name='bike',
            name='owner',
            field=models.ForeignKey(related_name='owned_bikes_set', blank=True, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True),
        ),
    ]
