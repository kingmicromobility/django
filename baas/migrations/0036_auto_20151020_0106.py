# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0035_auto_20151019_2227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbackeventdetail',
            name='event',
            field=models.ForeignKey(related_name='detail_set', to='baas.FeedbackEvent', on_delete=models.CASCADE),
        ),
    ]
