# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0017_auto_20151014_0557'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, blank=True)),
                ('started_at', models.DateTimeField()),
                ('expires_at', models.DateTimeField()),
                ('canceled_at', models.DateTimeField(null=True, blank=True)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
                ('renter', models.ForeignKey(to='baas.Person', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('guid', models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True)),
                ('createdAt', models.DateTimeField(auto_now_add=True, null=True)),
                ('lastModified', models.DateTimeField(db_index=True, auto_now=True, null=True)),
                ('deleted', models.BooleanField(default=False)),
                ('started_at', models.DateTimeField()),
                ('ended_at', models.DateTimeField()),
                ('ambassador', models.ForeignKey(related_name='session_bike_ambassador', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True)),
                ('bike', models.ForeignKey(to='baas.Bike', on_delete=models.CASCADE)),
                ('ended_event', models.OneToOneField(related_name='session_ended_lock_event', blank=True, to='baas.LockEvent', on_delete=models.CASCADE, null=True)),
                ('owner', models.ForeignKey(related_name='session_bike_owner', blank=True, to='baas.Person', on_delete=models.CASCADE, null=True)),
                ('renter', models.ForeignKey(related_name='session_bike_renter', to='baas.Person', on_delete=models.CASCADE)),
                ('reservation', models.ForeignKey(to='baas.Reservation', on_delete=models.CASCADE)),
                ('started_event', models.OneToOneField(related_name='session_started_lock_event', to='baas.LockEvent', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='trip',
            name='session',
            field=models.ForeignKey(default=0, to='baas.Session', on_delete=models.CASCADE),
            preserve_default=False,
        ),
    ]
