# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0029_auto_20151019_1909'),
    ]

    operations = [
        migrations.RenameField(
            model_name='feedbackprompt',
            old_name='english',
            new_name='default',
        ),
    ]
