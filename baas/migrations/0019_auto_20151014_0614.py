# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0018_auto_20151014_0557'),
    ]

    operations = [
        migrations.AlterField(
            model_name='session',
            name='ended_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
