# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0005_auto_20151005_2231'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bike',
            name='id',
        ),
        migrations.RemoveField(
            model_name='bikelocation',
            name='id',
        ),
        migrations.RemoveField(
            model_name='bikelockaction',
            name='id',
        ),
        migrations.RemoveField(
            model_name='bikerelationship',
            name='id',
        ),
        migrations.RemoveField(
            model_name='market',
            name='id',
        ),
        migrations.RemoveField(
            model_name='person',
            name='id',
        ),
        migrations.RemoveField(
            model_name='reservation',
            name='id',
        ),
        migrations.RemoveField(
            model_name='session',
            name='id',
        ),
        migrations.AddField(
            model_name='bike',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelocation',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikelockaction',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bikerelationship',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='market',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='reservation',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='session',
            name='guid',
            field=models.CharField(db_index=True, max_length=40, serialize=False, primary_key=True, blank=True),
            preserve_default=True,
        ),
    ]
