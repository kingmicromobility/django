# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0019_auto_20151014_0614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trip',
            name='ended_event',
            field=models.ForeignKey(related_name='session_ended_event', blank=True, to='baas.LockEvent', on_delete=models.CASCADE, null=True),
        ),
    ]
