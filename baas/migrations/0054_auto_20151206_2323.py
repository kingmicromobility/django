# -*- coding: utf-8 -*-
# Generated by Django 1.9b1 on 2015-12-06 23:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('baas', '0053_lockevent_lock'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lockevent',
            name='lock',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='baas.Lock'),
        ),
    ]
