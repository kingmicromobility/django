import locale
import os
from django.core.mail import EmailMessage
from django_rq import job


@job('default', result_ttl=0)
def send_welcome_email(person):
    msg = EmailMessage(
        subject="Welcome to Baas Bikes!",
        from_email="hello@baasbikes.com",
        to=[person.user.email])
    msg.use_template_subject = True
    msg.use_template_from = True
    msg.merge_language = 'handlebars'
    msg.template_name = "welcome-email-template"
    msg.template_content = {}
    msg.global_merge_vars = {}
    msg.merge_vars = {
        person.user.email: {
            'first_name': person.first_name,
            'last_name': person.last_name}
    }
    msg.send()


@job('default', result_ttl=0)
def send_session_receipt_email(session):
    locale.setlocale(locale.LC_ALL, '')
    bike_details = {
        'name': session.bike.name}
    for detail in session.bike.detail_set.all():
        bike_details[detail.name] = detail.value
    merge_vars = {
        'session_end_date': session.ended_at.strftime('%A, %B %-m, %Y'),
        'first_name': session.renter.first_name,
        'last_name': session.renter.last_name,
        'route_map_url': 'http://%s/%s' % (os.environ.get('S3_STATICS_BUCKET'), session.get_s3_path_to_route_image()),
        'total_distance': '%.2f' % session.total_distance_miles,
        'total_duration_hours': str(session.total_duration_seconds / 3600).zfill(2),
        'total_duration_minutes': str((session.total_duration_seconds % 3600) / 60).zfill(2),
        'total_duration_seconds': str(session.total_duration_seconds % 60).zfill(2),
        'total_price': locale.currency(
            session.financial_transaction.total_amount / 100, grouping=True),
        'line_items': [],
        'payment_attempts': [],
        'bike_name': bike_details['name'],
        'bike_brand': session.bike.brand.name,
        'bike_model': bike_details['model'],
        'bike_color': bike_details['color'],
        'bike_gears': bike_details['gears'],
        'started_at': session.started_at.strftime('%A, %B %-m at %-H:%-M'),
        'ended_at': session.ended_at.strftime('%A, %B %-m at %-H:%-M')}
    for line_item in session.financial_transaction.line_item_set.order_by('created_at','description').all():
        merge_vars['line_items'].append({
            'description': line_item.description,
            'amount': locale.currency(line_item.amount / 100, grouping=True)})
    for payment_attempt in session.financial_transaction.payment_attempt_set.all():
        if payment_attempt.successful:
            merge_vars['payment_attempts'].append({
                'description': payment_attempt.payment_method_description,
                'amount': locale.currency(
                    payment_attempt.amount / 100, grouping=True)})
    msg = EmailMessage(
        subject="Session Complete!",
        from_email="hello@baasbikes.com",
        to=[session.renter.user.email])
    msg.use_template_subject = True
    msg.use_template_from = True
    msg.merge_language = 'handlebars'
    msg.template_name = "session-receipt-template"
    msg.template_content = {}
    msg.global_merge_vars = {}
    msg.merge_vars = {session.renter.user.email: merge_vars}
    msg.send()


@job('default', result_ttl=0)
def send_password_reset_email(person, temp_password):
    msg = EmailMessage(
        subject="Your Temporary Password",
        from_email="hello@baasbikes.com",
        to=[person.user.email])
    msg.use_template_subject = True
    msg.use_template_from = True
    msg.merge_language = 'handlebars'
    msg.template_name = "password-reset-template"
    msg.template_content = {}
    msg.global_merge_vars = {}
    msg.merge_vars = {
        person.user.email: {
            'first_name': person.first_name,
            'last_name': person.last_name,
            'temporary_password': temp_password}
    }
    msg.send()
