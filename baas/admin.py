from django.contrib import admin
from django.contrib.gis import forms
from django.utils.html import format_html
from django.contrib.gis import admin as geoadmin
from baas.models import *
from baas.latLonWidgetHelper import LatLongField
from baas.baas_shortcuts import googleMapsURLFromLocation, googleMapsLinkFromLocation


class MarketAdmin(geoadmin.GeoModelAdmin):
    pass


class BikeAdminForm(forms.ModelForm):
    last_location = LatLongField()


class BikeDetailInline(admin.TabularInline):
    model = BikeDetail
    fields = ('guid', 'name', 'value')
    exclude = ('guid', )


class BikeAdmin(geoadmin.GeoModelAdmin):
    list_display = ('name', 'market', 'listing_type', 'status', 'total_sessions',
                    'current_renter', 'view_link')
    list_filter = ('market', 'listing_type', 'status',)
    search_fields = ('name',)
    ordering = ('market', 'listing_type', 'name',)
    inlines = [
        BikeDetailInline,
    ]
    # form = BikeAdminForm

    def total_sessions(self, obj):
        return obj.session_set.count()

    def view_link(self, obj):
        return format_html(googleMapsLinkFromLocation(obj.last_location))
    view_link.short_description = 'see in Google Maps'


class ReservationAdminForm(forms.ModelForm):
    location = LatLongField()


class ReservationAdmin(geoadmin.GeoModelAdmin):
    list_display = ('bike', 'renter', 'started_at', 'expires_at',
                    'canceled_at', 'session_started_at',)
    ordering = ('-created_at',)
    search_fields = ('bike__name', 'renter__first_name', 'renter__last_name')
    form = ReservationAdminForm

    def render_change_form(self, request, context, *args, **kwargs):
        # here we define a custom template
        self.change_form_template = 'custom_admin/change_form_with_optional_google_maps_link.html'
        reservation = kwargs['obj']
        if reservation is not None:
            extra = {
                'google_maps_link': googleMapsURLFromLocation(
                    reservation.location)
            }
            context.update(extra)
        return super(ReservationAdmin, self).render_change_form(
            request, context, *args, **kwargs)


class SessionAdmin(admin.ModelAdmin):
    list_display = ('bike', 'renter', 'started_at', 'ended_at',
                    'total_duration_seconds', 'total_distance_miles')
    list_filter = ('bike', 'renter',)
    search_fields = ('bike__name', 'renter__first_name', 'renter__last_name',)
    ordering = ('-started_at',)


class TripAdmin(admin.ModelAdmin):
    list_filter = ('started_event__person', 'started_event__bike',)
    search_fields = ('started_event__person__first_name', 'started_event__person__last_name',
                     'started_event__bike__name',)
    ordering = ('-started_event__time',)


class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'phone_number',
                    'original_market', 'get_significant_sessions',
                    'created_at', 'attributed_to')
    list_filter = ('original_market',)
    search_fields = ('first_name', 'last_name', 'user__email')
    ordering = ('-created_at',)

    def get_significant_sessions(self, obj):
        return obj.significant_sessions().count()
    get_significant_sessions.short_description = 'Significant Sessions'

    def attributed_to(self, obj):
        channels = obj.marketing_channel_attributions_all()
        return list(map(lambda channel: str(channel.name), channels))
    attributed_to.short_description = 'Attributed To'

    def email(self, obj):
        return str(obj.user.email)


class FeedbackEventAdmin(admin.ModelAdmin):
    list_display = ('bike', 'reporter', 'star_rating', 'feedback_type',
                    'detail_summary', 'created_at')
    list_filter = ('star_rating', 'bike', 'reporter',)
    search_fields = ('bike__name', 'reporter__first_name',
                     'reporter__last_name')
    ordering = ('-created_at',)

    def feedback_type(self, obj):
        return str(obj.prompts.name) if obj.prompts else None
    feedback_type.short_description = 'Type'

    def detail_summary(self, obj):
        return list(map(lambda detail: str(detail.value), obj.detail_set.all()))
    detail_summary.short_description = 'Detail Summary'


class AttributionChannelAdmin(admin.ModelAdmin):
    list_display = ('name', 'attribution_code', 'get_people', 'get_riders',
                    'get_repeats')
    ordering = ('name',)

    def get_people(self, obj):
        return len(obj.attributed_people_with_payment_method())
    get_people.short_description = 'People'

    def get_riders(self, obj):
        return len(obj.attributed_active_riders())
    get_riders.short_description = 'Riders'

    def get_repeats(self, obj):
        return len(obj.attributed_repeat_riders())
    get_repeats.short_description = "Repeat Riders"


class LockEventAdmin(admin.ModelAdmin):
    list_display = ('bike', 'event_type', 'person', 'time', 'outside_market',
                    'view_link')
    list_filter = ('event_type', 'bike', 'person',)
    search_fields = ('bike__name', 'person__first_name', 'person__last_name')
    ordering = ('-time',)

    def view_link(self, obj):
        return format_html(googleMapsLinkFromLocation(obj.location))
    view_link.short_description = 'see in Google Maps'


class LockAdmin(admin.ModelAdmin):
    list_display = ('type', 'baas_lock_id', 'battery_remaining', 'bike',)
    list_filter = ('type',)
    search_fields = ('bike__name', 'baas_lock_id',)
    ordering = ('baas_lock_id',)


admin.site.register(Person, PersonAdmin)
admin.site.register(Authority)
admin.site.register(AmbassadorDetail)
admin.site.register(AttributionChannel, AttributionChannelAdmin)
admin.site.register(Market, MarketAdmin)
admin.site.register(Lock, LockAdmin)
admin.site.register(Bike, BikeAdmin)
admin.site.register(BikeBrand)
admin.site.register(BikeStyle)
admin.site.register(BikeSize)
admin.site.register(BikeDetail)
admin.site.register(BikeImage)
admin.site.register(BikeImageSet)
admin.site.register(LockEvent, LockEventAdmin)
admin.site.register(Reservation, ReservationAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Trip, TripAdmin)
admin.site.register(FeedbackEvent, FeedbackEventAdmin)
admin.site.register(FeedbackEventDetail)
admin.site.register(FeedbackPrompt)
admin.site.register(FeedbackPromptSet)
