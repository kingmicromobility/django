from datetime import timedelta
from django.test import TestCase
from django.core.exceptions import ValidationError
from django.contrib.gis.geos.point import Point
from baas.models import InvalidTripStateException
from baas import models, ws_notifications, mailers, payment_gateway
from baas import mailing_list_manager
from baas import factories as f
from unittest.mock import MagicMock

mailers.send_welcome_email = MagicMock()
mailers.send_session_receipt_email = MagicMock()
payment_gateway.create_or_update_customer = MagicMock()
payment_gateway.list_payment_methods = MagicMock()
payment_gateway.create_payment_method = MagicMock()
payment_gateway.create_transaction = MagicMock()
ws_notifications.bike_status = MagicMock()
ws_notifications.session_status = MagicMock()
mailing_list_manager.add_person_to_list = MagicMock()


class MarketTestCase(TestCase):
    def setUp(self):
        self.market = f.MarketFactory.build()

    def test_string_representation(self):
        self.assertEqual(str(self.market), self.market.name)

    def test_point_is_in_market(self):
        self.assertTrue(self.market.contains(Point(-77.01, 38.92)))

    def test_point_is_outside_market(self):
        self.assertFalse(self.market.contains(Point(-97.01, 48.92)))


class BikeTestCase(TestCase):
    def setUp(self):
        self.bike = f.BikeFactory.build()
        self.nowhere_bike = f.BikeFactory.build(last_location=None)

    def test_persists_location_as_point(self):
        self.assertIsInstance(self.bike.last_location, Point)

    def test_last_latitude(self):
        self.assertEqual(self.bike.last_latitude(), 38.909784928)
        self.assertEqual(self.nowhere_bike.last_latitude(), None)

    def test_last_longitude(self):
        self.assertEqual(self.bike.last_longitude(), -77.027263837)
        self.assertEqual(self.nowhere_bike.last_longitude(), None)

    def test_within_own_market(self):
        self.assertTrue(self.bike.within_own_market())

    def test_not_within_own_market(self):
        self.bike.last_location = Point(-78.027, 39.009)
        self.assertFalse(self.bike.within_own_market())

    def test_string_representation(self):
        self.assertEqual(str(self.bike), self.bike.name)

    def test_has_lock(self):
        self.assertIsNotNone(self.bike.lock)


class BikeBrandTestCase(TestCase):
    def setUp(self):
        self.brand = f.BikeBrandFactory.build()

    def test_string_representation(self):
        self.assertEqual(str(self.brand), self.brand.name)


class BikeSizeTestCase(TestCase):
    def setUp(self):
        self.size = f.BikeSizeFactory.build()

    def test_string_representation(self):
        self.assertEqual(str(self.size), self.size.name)


class BikeStyleTestCase(TestCase):
    def setUp(self):
        self.style = f.BikeStyleFactory.build()

    def test_string_representation(self):
        self.assertEqual(str(self.style), self.style.name)


class BikeDetailTestCase(TestCase):
    def setUp(self):
        self.detail = f.BikeDetailFactory.build()

    def test_string_representation(self):
        self.assertEqual(str(self.detail), '(Test Bike) Fenders : True')


class BikeImageSetTestCase(TestCase):
    def setUp(self):
        self.image_set = f.BikeImageSetFactory.build()

    def test_string_representation(self):
        self.assertEqual(str(self.image_set), self.image_set.name)


class BikeImageTestCase(TestCase):
    def setUp(self):
        self.image = f.BikeImageFactory.build()

    def test_string_representation(self):
        self.assertEqual(str(self.image), self.image.name)

    def test_has_image_set(self):
        self.assertIsNotNone(self.image.set)

    def test_has_key(self):
        self.assertIsNotNone(self.image.key)

    def test_has_name(self):
        self.assertIsNotNone(self.image.name)


class LockTestCase(TestCase):
    def setUp(self):
        self.lock = f.LockFactory()

    def test_string_representation(self):
        self.assertEqual(
            str(self.lock),
            'baas.lock.%s.%s' % (self.lock.type, str(self.lock.baas_lock_id)))

    def test_has_type(self):
        self.assertEqual(self.lock.type, 'seat-post')

    def test_has_salt(self):
        self.assertEqual(self.lock.salt, '17')

    def test_has_battery_remaining(self):
        self.assertEqual(self.lock.battery_remaining, 75.0)


class LockEventTestCase(TestCase):
    def test_string_representation(self):
        r = f.ReservationFactory()
        unlock_event = f.UnlockEventFactory(bike=r.bike, person=r.renter)
        self.assertEqual(
            str(unlock_event),
            'Freddie Mercury unlocked Test Bike at %s near %s' %
            (unlock_event.time, unlock_event.location))

    def test_captures_lock_attribute_from_bike(self):
        r = f.ReservationFactory()
        unlock_event = f.UnlockEventFactory(bike=r.bike, person=r.renter)
        self.assertEqual(unlock_event.lock, r.bike.lock)

    def test_auto_populate_time(self):
        r = f.ReservationFactory()
        unlock_event = f.UnlockEventFactory(bike=r.bike, person=r.renter)
        self.assertIsNotNone(unlock_event.time)

    def test_unlock_with_open_reservation_and_no_open_session(self):
        r = f.ReservationFactory()
        unlock_event = f.UnlockEventFactory(bike=r.bike, person=r.renter)
        session = r.renter.open_sessions_for_bike(r.bike).first()
        bike = models.Bike.objects.get(pk=r.bike.guid)
        self.assertEqual(session.trip_set.count(), 1)
        self.assertEqual(session.started_event.guid, str(unlock_event.guid))
        self.assertEqual(
            session.trip_set.first().started_event.guid,
            str(unlock_event.guid))
        self.assertEqual(bike.status, 'active')

    def test_unlock_with_open_session_and_no_open_trips(self):
        r = f.ReservationFactory()
        f.UnlockEventFactory(bike=r.bike, person=r.renter)
        f.LockEventFactory(bike=r.bike, person=r.renter)
        unlock_event = f.UnlockEventFactory(bike=r.bike, person=r.renter)
        open_session = r.renter.open_sessions_for_bike(r.bike).first()
        last_trip    = open_session.trip_set.order_by('created_at').last()
        self.assertEqual(open_session.trip_set.count(), 2)
        self.assertEqual(last_trip.started_event.guid, str(unlock_event.guid))
        self.assertIsNone(last_trip.ended_event)
        self.assertEqual(open_session.bike.status, 'active')

    def test_unlock_with_open_session_and_open_trip(self):
        r = f.ReservationFactory()
        f.UnlockEventFactory(bike=r.bike, person=r.renter)
        with self.assertRaises(InvalidTripStateException):
            f.UnlockEventFactory(bike=r.bike, person=r.renter)
        self.assertEqual(r.bike.status, 'active')

    def test_unlock_with_no_open_session_and_no_open_reservation(self):
        bike = f.BikeFactory()
        with self.assertRaises(ValidationError):
            f.UnlockEventFactory(bike=bike)
        self.assertEqual(bike.status, 'available')

    def test_lock_with_open_session_and_no_open_trip(self):
        r = f.ReservationFactory()
        first_unlock_event = f.UnlockEventFactory(bike=r.bike, person=r.renter)
        bike = models.Bike.objects.get(guid=r.bike.guid)
        first_lock_event = f.LockEventFactory(bike=bike, person=r.renter)
        bike = models.Bike.objects.get(guid=r.bike.guid)
        with self.assertRaises(ValidationError):
            f.LockEventFactory(bike=bike, person=r.renter)
        bike = models.Bike.objects.get(guid=r.bike.guid)
        open_session = r.renter.open_sessions_for_bike(r.bike).first()
        last_trip = open_session.trip_set.order_by('created_at').last()
        self.assertEqual(open_session.trip_set.count(), 1)
        self.assertEqual(
            last_trip.started_event.guid, str(first_unlock_event.guid))
        self.assertEqual(
            last_trip.ended_event.guid, str(first_lock_event.guid))
        self.assertEqual(bike.status, 'held')

    def test_lock_with_open_session_and_open_trip(self):
        r = f.ReservationFactory()
        unlock_event = f.UnlockEventFactory(bike=r.bike, person=r.renter)
        lock_event = f.LockEventFactory(bike=r.bike, person=r.renter)
        open_session = r.renter.open_sessions_for_bike(r.bike).first()
        last_trip = open_session.trip_set.order_by('created_at').last()
        self.assertEqual(open_session.trip_set.count(), 1)
        self.assertEqual(last_trip.started_event.guid, str(unlock_event.guid))
        self.assertEqual(last_trip.ended_event.guid, str(lock_event.guid))
        self.assertEqual(open_session.bike.status, 'held')

    def test_lock_with_reservation_but_no_open_session(self):
        r = f.ReservationFactory()
        with self.assertRaises(ValidationError):
            f.LockEventFactory(bike=r.bike, person=r.renter)
        self.assertEqual(r.renter.open_sessions_for_bike(r.bike).count(), 0)
        self.assertEqual(r.bike.status, 'reserved')


class PersonTestCase(TestCase):
    def setUp(self):
        self.person = f.PersonFactory()
        self.bike = f.BikeFactory()

    def test_string_representation(self):
        self.assertEqual(
            str(self.person),
            '%s %s' % (self.person.first_name, self.person.last_name))

    def test_open_reservations_for_bike_active_reservation(self):
        f.ReservationFactory(renter=self.person, bike=self.bike)
        self.assertEqual(
            self.person.open_reservations_for_bike(self.bike).count(), 1)

    def test_has_open_reservations_or_sessions_active_reservation(self):
        f.ReservationFactory(renter=self.person, bike=self.bike)
        self.assertTrue(self.person.has_open_reservations_or_sessions())

    def test_open_reservations_for_bike_canceled_reservation(self):
        f.CanceledReservationFactory(renter=self.person, bike=self.bike)
        self.assertEqual(
            self.person.open_reservations_for_bike(self.bike).count(), 0)

    def test_has_open_reservations_or_sessions_canceled_reservation(self):
        f.CanceledReservationFactory(renter=self.person, bike=self.bike)
        self.assertFalse(self.person.has_open_reservations_or_sessions())

    def test_open_reservations_for_bike_expired_reservation(self):
        f.ExpiredReservationFactory(renter=self.person, bike=self.bike)
        self.assertEqual(
            self.person.open_reservations_for_bike(self.bike).count(), 0)

    def test_has_open_reservations_or_sessions_expired_reservation(self):
        f.ExpiredReservationFactory(renter=self.person, bike=self.bike)
        self.assertFalse(self.person.has_open_reservations_or_sessions())

    def test_open_sessions_for_bike(self):
        f.ReservationFactory(renter=self.person, bike=self.bike)
        f.UnlockEventFactory(person=self.person, bike=self.bike)
        self.assertEqual(
            self.person.open_sessions_for_bike(self.bike).count(), 1)


class ReservationTestCase(TestCase):
    def setUp(self):
        self.reservation = f.ReservationFactory()

    def test_auto_populate_expires_at(self):
        expectation = self.reservation.started_at + timedelta(minutes=15)
        self.assertEqual(self.reservation.expires_at, expectation)

    def test_cancel(self):
        self.reservation.cancel()
        self.assertIsNotNone(self.reservation.canceled_at)
        self.assertEqual(self.reservation.bike.status, 'available')

    def test_bike_status_is_reserved(self):
        self.assertEqual(self.reservation.bike.status, 'reserved')

    def test_reserved_bike_cannot_be_reserved_again(self):
        different_person = f.SecondPersonFactory()
        with self.assertRaises(ValidationError):
            f.ReservationFactory(
                bike=self.reservation.bike, renter=different_person)

    def test_only_one_reservation_per_person(self):
        with self.assertRaises(ValidationError):
            f.ReservationFactory(renter=self.reservation.renter)

    def test_string_representation(self):
        self.assertEqual(
            str(self.reservation),
            'Reservation of "%s" by renter "%s" at "%s" initiated near "%s"' %
            (self.reservation.bike, self.reservation.renter,
             self.reservation.started_at, self.reservation.location))

    def test_has_session_started_at_if_session_started(self):
        f.UnlockEventFactory(
            bike=self.reservation.bike, person=self.reservation.renter)
        refreshed_reservation = models.Reservation.objects.last()
        self.assertIsNotNone(refreshed_reservation.session_started_at)


class SessionTestCase(TestCase):
    def setUp(self):
        r = f.ReservationFactory()
        f.UnlockEventFactory(bike=r.bike, person=r.renter)
        self.session = r.session_set.first()

    def test_string_representation(self):
        self.assertEqual(
            str(self.session),
            'Session with bike "Test Bike" by renter "%s %s" at "%s"' %
            (self.session.renter.first_name,
                self.session.renter.last_name,
                self.session.started_at))


class TripTestCase(TestCase):
    def setUp(self):
        self.r = f.ReservationFactory()
        f.UnlockEventFactory(bike=self.r.bike, person=self.r.renter)

    def test_string_representation_for_finished_trip(self):
        f.LockEventFactory(bike=self.r.bike, person=self.r.renter)
        session = self.r.session_set.first()
        trip = session.trip_set.first()
        self.assertEqual(
            str(trip),
            'Trip starting with %s and ending with %s during %s' %
            (str(trip.started_event), str(trip.ended_event), str(trip.session))
        )

    def test_string_representation_for_unfinished_trip(self):
        session = self.r.session_set.first()
        trip    = session.trip_set.first()
        self.assertEqual(
            str(trip),
            'Unfinished trip starting with %s during %s' %
            (str(trip.started_event), str(trip.session)))


class FeedbackPromptTestCase(TestCase):
    def setUp(self):
        self.feedback_prompt = f.FeedbackPromptFactory()

    def test_string_representation(self):
        self.assertEqual(
            str(self.feedback_prompt), self.feedback_prompt.default)


class FeedbackPromptSetTestCase(TestCase):
    def setUp(self):
        self.feedback_prompt_set = f.FeedbackPromptSetFactory()

    def test_string_representation(self):
        self.assertEqual(
            str(self.feedback_prompt_set), self.feedback_prompt_set.name)


class FeedbackEventTestCase(TestCase):
    def setUp(self):
        self.feedback_event = f.FeedbackEventFactory()

    def test_string_representation(self):
        expectation = 'Feedback about %s by %s at %s' % (
            str(self.feedback_event.bike),
            str(self.feedback_event.reporter),
            str(self.feedback_event.time))
        self.assertEqual(str(self.feedback_event), expectation)


class FeedbackEventDetailTestCase(TestCase):
    def setUp(self):
        self.feedback_event_detail = f.FeedbackEventDetailFactory()

    def test_string_representation(self):
        self.assertEqual(
            str(self.feedback_event_detail),
            '%s : %s' % (str(self.feedback_event_detail.prompt),
                         self.feedback_event_detail.value))
