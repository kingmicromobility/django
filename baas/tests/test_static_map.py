import polyline
from django.test import TestCase
from baas.static_map import GoogleStaticMapMarker
from baas.static_map import GoogleStaticMapPolyline
from baas.static_map import GoogleStaticMap


class GoogleStaticMapMarkerTestCase(TestCase):

    def setUp(self):
        self.size = 'large'
        self.color = 'red'
        self.label = '1'
        self.locations = ['33.45,-80.25', '33.50,-80.50']
        self.marker = GoogleStaticMapMarker(
            self.size, self.color, self.label, self.locations)

    def test_assigns_size_from_constructor(self):
        self.assertEqual(self.size, self.marker.size)

    def test_assigns_color_from_constructor(self):
        self.assertEqual(self.color, self.marker.color)

    def test_assigns_label_from_constructor(self):
        self.assertEqual(self.label, self.marker.label)

    def test_assigns_locations_from_constructor(self):
        self.assertEqual(self.locations, self.marker.locations)

    def test_add_location(self):
        location = '33.55,-80.75'
        self.marker.add_location(location)
        self.assertEqual(location, self.marker.locations[-1])

    def test_as_url_param(self):
        expected_template = 'markers=size:%s|color:%s|label:%s|%s'
        expected_values = (
            self.size,
            self.color,
            self.label,
            '|'.join(self.locations))
        expected_param = expected_template % expected_values
        self.assertEqual(expected_param, self.marker.as_url_param())


class GoogleStaticMapPolylineTestCase(TestCase):

    def setUp(self):
        self.weight = 3
        self.color = 'orange'
        self.locations = [(33.45, -80.25), (33.50, -80.50)]
        self.polyline = GoogleStaticMapPolyline(
            self.weight, self.color, self.locations)

    def test_assigns_weight_from_constructor(self):
        self.assertEqual(self.weight, self.polyline.weight)

    def test_assigns_color_from_constructor(self):
        self.assertEqual(self.color, self.polyline.color)

    def test_assigns_locations_from_constructor(self):
        self.assertEqual(self.locations, self.polyline.locations)

    def test_add_locatioN(self):
        location = (33.55, -80.75)
        self.polyline.add_location(location)
        self.assertEqual(location, self.polyline.locations[-1])

    def test_as_url_param(self):
        expected_template = 'path=weight:%s|color:%s|enc:%s'
        expected_values = (
            self.weight,
            self.color,
            polyline.encode(self.locations))
        expected_param = expected_template % expected_values
        self.assertEqual(expected_param, self.polyline.as_url_param())


class GoogleStaticMapTestCase(TestCase):

    def setUp(self):
        self.api_key = 'ABCDEFG'
        self.map = GoogleStaticMap(self.api_key)

    def customizeSize(self):
        custom_width = 2048
        custom_height = 1024
        self.map.set_size(custom_width, custom_height)
        return custom_width, custom_height

    def addMarker(self):
        locations = ['33.45,-80.25', '33.50,-80.50']
        marker = GoogleStaticMapMarker('large', 'red', '1', locations)
        self.map.add_marker(marker)
        return marker

    def addPolyline(self):
        locations = [(33.45, -80.25), (33.50, -80.50)]
        polyline = GoogleStaticMapPolyline(3, 'orange', locations)
        self.map.add_polyline(polyline)
        return polyline

    def test_assigns_endpoint_in_constructor(self):
        endpoint = 'https://maps.googleapis.com/maps/api/staticmap'
        self.assertEqual(endpoint, self.map.endpoint)

    def test_assigns_api_key_from_constructor(self):
        self.assertEqual(self.api_key, self.map.api_key)

    def test_assigns_default_width_in_constructor(self):
        self.assertEqual(512, self.map.width)

    def test_assigns_default_height_in_constructor(self):
        self.assertEqual(512, self.map.height)

    def test_assigns_empty_marker_set_in_constructor(self):
        self.assertEqual([], self.map.markers)

    def test_assigns_empty_polyline_set_in_constructor(self):
        self.assertEqual([], self.map.polylines)

    def test_sets_map_dimensions(self):
        custom_width, custom_height = self.customizeSize()
        self.assertEqual(custom_width, self.map.width)
        self.assertEqual(custom_height, self.map.height)

    def test_add_marker(self):
        marker = self.addMarker()
        self.assertEqual(marker, self.map.markers[-1])

    def test_add_polyline(self):
        polyline = self.addPolyline()
        self.assertEqual(polyline, self.map.polylines[-1])

    def test_as_url(self):
        self.addMarker()
        self.addPolyline()
        self.customizeSize()
        url = 'https://maps.googleapis.com/maps/api/staticmap?size=2048x1024&markers=size:large|color:red|label:1|33.45,-80.25|33.50,-80.50&path=weight:3|color:orange|enc:oedkEnyxhNowHnyo@&key=ABCDEFG'
        self.assertEqual(url, self.map.as_url())
