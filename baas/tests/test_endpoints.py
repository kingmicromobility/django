import json
from datetime import datetime
from baas import models, ws_notifications, mailers, payment_gateway
from baas import mailing_list_manager
from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.contrib.gis.geos.point import Point
from baas import factories as f
from unittest.mock import patch, MagicMock

mailers.send_welcome_email = MagicMock()
mailers.send_session_receipt_email = MagicMock()
mailers.send_password_reset_email = MagicMock()
payment_gateway.create_or_update_customer = MagicMock()
payment_gateway.list_payment_methods = MagicMock()
payment_gateway.create_payment_method = MagicMock()
payment_gateway.create_transaction = MagicMock()
ws_notifications.bike_status = MagicMock()
ws_notifications.session_status = MagicMock()
mailing_list_manager.add_person_to_list = MagicMock()


class AccountCreationTestCase(TestCase):
    def setUp(self):
        self.attr_ch1 = f.AttributionChannelOne()
        self.attr_ch2 = f.AttributionChannelTwo()
        self.person = f.PersonFactory.build()
        self.resp = Client().post(
            '/people/',
            content_type='application/json',
            data=json.dumps({
                'first_name': self.person.first_name,
                'last_name': self.person.last_name,
                'phone_number': self.person.phone_number,
                'self_reported_channel_code': self.attr_ch1.attribution_code,
                'auto_reported_channel_code': self.attr_ch2.attribution_code,
                'user': {
                    'email': self.person.user.email,
                    'password': self.person.user.password}}))
        self.resp_data = json.loads(self.resp.content)
        self.db_person = models.Person.objects.get(
            first_name=self.person.first_name, last_name=self.person.last_name)

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_contains_guid(self):
        self.assertTrue('guid' in self.resp_data)

    def test_contains_first_name(self):
        self.assertTrue('first_name' in self.resp_data)

    def test_contains_last_name(self):
        self.assertTrue('last_name' in self.resp_data)

    def test_contains_phone_number(self):
        self.assertTrue('phone_number' in self.resp_data)

    def test_contains_user(self):
        self.assertTrue('user' in self.resp_data)

    def test_contains_email(self):
        self.assertTrue('email' in self.resp_data['user'])

    def test_omits_password(self):
        self.assertFalse('password' in self.resp_data['user'])

    def test_creates_person_record(self):
        self.assertIsNotNone(
            models.Person.objects.get(first_name=self.person.first_name))

    def test_creates_user_record(self):
        self.assertIsNotNone(
            get_user_model().objects.get(email=self.person.user.email))

    def test_attributes_person_to_channels(self):
        self.assertTrue(
            self.db_person in self.attr_ch1.self_reported_people.all())
        self.assertTrue(
            self.db_person in self.attr_ch2.auto_reported_people.all())


class AuthenticatedTestCase(TestCase):
    class Meta:
        abstract = True

    def setUp(self, is_staff=False):
        self.person = f.PersonFactory(is_staff=is_staff)
        self.resp = Client().post(
            '/api-auth-token/',
            content_type='application/json',
            data=json.dumps({
                'email': self.person.user.email,
                'password': 'correctpassword'}))
        self.token = 'Token %s' % json.loads(self.resp.content)['token']


class AuthenticationWithValidCredentialsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(AuthenticationWithValidCredentialsTestCase, self).setUp()
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_token(self):
        self.assertTrue('token' in self.resp_data)

    def test_contains_person(self):
        self.assertTrue('person' in self.resp_data)

    def test_contains_first_name(self):
        self.assertTrue('first_name' in self.resp_data['person'])

    def test_contains_last_name(self):
        self.assertTrue('last_name' in self.resp_data['person'])

    def test_contains_phone_number(self):
        self.assertTrue('phone_number' in self.resp_data['person'])

    def test_contains_payment_gateway_id(self):
        self.assertTrue('payment_gateway_id' in self.resp_data['person'])

    def test_contains_email(self):
        self.assertTrue('email' in self.resp_data['person']['user'])


class AuthenticationWithInvalidCredentialsTestCase(TestCase):
    def setUp(self):
        self.person = f.PersonFactory()
        self.resp = Client().post(
            '/api-auth-token/',
            content_type='application/json',
            data=json.dumps({
                'email': self.person.user.email,
                'password': 'wrongpassword'}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_400(self):
        self.assertEqual(self.resp.status_code, 400)

    def test_omits_token(self):
        self.assertFalse('token' in self.resp_data)


class UpdateAccountTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(UpdateAccountTestCase, self).setUp()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/people/%s/' % str(self.person.guid),
            content_type='application/json',
            data=json.dumps({
                'first_name': 'Jane',
                'phone_number': '(111) 111-1111',
                'user': {
                    'email': 'jane@doe.com'}}))
        self.resp_data = json.loads(self.resp.content)
        self.person = models.Person.objects.last()

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_updates_person_first_name(self):
        self.assertEqual(self.person.first_name, 'Jane')

    def test_updates_person_phone_number(self):
        self.assertEqual(self.person.phone_number, '(111) 111-1111')

    def test_updates_user_email(self):
        self.assertEqual(self.person.user.email, 'jane@doe.com')

    def test_response_contains_token(self):
        self.assertIsNotNone(self.resp_data['token'])


class ReuseEmailTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(ReuseEmailTestCase, self).setUp()
        self.resp1 = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/people/%s/' % str(self.person.guid),
            content_type='application/json',
            data=json.dumps({'user': {'email': 'jane@doe.com'}}))
        self.resp1_data = json.loads(self.resp1.content)
        self.new_token = 'Token %s' % self.resp1_data['token']
        self.resp2 = Client(HTTP_AUTHORIZATION=self.new_token).patch(
            '/people/%s/' % str(self.person.guid),
            content_type='application/json',
            data=json.dumps({'user': {'email': 'fmercury@queen.com'}}))
        self.resp2_data = json.loads(self.resp2.content)
        self.person = models.Person.objects.last()

    def test_returns_200_on_first_change(self):
        self.assertEqual(self.resp1.status_code, 200)

    def test_returns_200_on_second_change(self):
        self.assertEqual(self.resp2.status_code, 200)

    def test_includes_new_auth_token(self):
        self.assertIsNotNone(self.resp1_data['token'])
        self.assertIsNotNone(self.resp2_data['token'])

    def test_changes_email_back_to_old_one(self):
        self.assertEqual(self.person.user.email, 'fmercury@queen.com')


class ChangePasswordWithValidPassword(AuthenticatedTestCase):
    def setUp(self):
        super(ChangePasswordWithValidPassword, self).setUp()
        self.old_password = models.Person.objects.last().user.password
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/people/%s/' % str(self.person.guid),
            content_type='application/json',
            data=json.dumps({
                'user': {
                    'old_password': 'correctpassword',
                    'new_password': 'new1password',
                    'confirm_password': 'new1password'}}))
        self.person = models.Person.objects.last()

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_password_has_been_changed(self):
        self.assertNotEqual(self.old_password, self.person.user.password)


class ChangePasswordWithInvalidPassword(AuthenticatedTestCase):
    def setUp(self):
        super(ChangePasswordWithInvalidPassword, self).setUp()
        self.old_password = models.Person.objects.last().user.password
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/people/%s/' % str(self.person.guid),
            content_type='application/json',
            data=json.dumps({
                'user': {
                    'old_password': 'wrong',
                    'new_password': 'new1password',
                    'confirm_password': 'new1password'}}))
        self.person = models.Person.objects.last()

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 400)

    def test_password_has_not_been_changed(self):
        self.assertEqual(self.old_password, self.person.user.password)


class ChangePasswordWithMismatchedPasswords(AuthenticatedTestCase):
    def setUp(self):
        super(ChangePasswordWithMismatchedPasswords, self).setUp()
        self.old_password = models.Person.objects.last().user.password
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/people/%s/' % str(self.person.guid),
            content_type='application/json',
            data=json.dumps({
                'user': {
                    'old_password': 'correctpassword',
                    'new_password': 'newpassword',
                    'confirm_password': 'doesnotmatch'}}))
        self.person = models.Person.objects.last()

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 400)

    def test_password_has_not_been_changed(self):
        self.assertEqual(self.old_password, self.person.user.password)


class ResetPasswordWithValidEmail(TestCase):
    def setUp(self):
        self.person = f.PersonFactory()
        self.old_password = models.Person.objects.last().user.password
        self.resp = Client().post(
            '/temporary_password/',
            content_type='application/json',
            data=json.dumps({
                'email': 'fmercury@queen.com'}))
        self.person = models.Person.objects.last()

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_password_has_been_changed(self):
        self.assertNotEqual(self.old_password, self.person.user.password)


class ResetPasswordWithInvalidEmail(TestCase):
    def setUp(self):
        self.person = f.PersonFactory()
        self.resp = Client().post(
            '/temporary_password/',
            content_type='application/json',
            data=json.dumps({
                'email': 'wrong@email.com'}))

    def test_returns_201_even_though_account_doesnt_exist(self):
        self.assertEqual(self.resp.status_code, 201)


class QueryBikesTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(QueryBikesTestCase, self).setUp()
        f.BikeFactory(last_location=Point(-77.500, 38.500), status='reserved')
        f.BikeFactory(last_location=Point(-77.500, 38.502))
        f.BikeFactory(last_location=Point(-77.500, 38.498))
        f.BikeFactory(last_location=Point(-79.500, 40.500))
        f.BikeFactory(last_location=Point(-105.500, 6.500))

    def test_query_bikes_by_lat_lon(self):
        resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/bikes/',
            content_type='application/json',
            data={'last_lat': 38.5, 'last_lon': -77.5})
        resp_data = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp_data['results']), 3)

    def test_query_bikes_by_lat_lon_radius(self):
        resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/bikes/',
            content_type='application/json',
            data={'last_lat': 38.5, 'last_lon': -77.5, 'radius': 500})
        resp_data = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp_data['results']), 4)

    def test_query_bikes_by_status(self):
        resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/bikes/',
            content_type='applicaton/json',
            data={'status': 'available'})
        resp_data = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp_data['results']), 4)


class UnauthenticatedShowMarketTestCase(TestCase):
    def setUp(self):
        self.market = f.MarketFactory()
        self.resp = Client().get(
            '/markets/%s/' % str(self.market.guid),
            content_type='application/json')
        print(self.resp.content)
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_returns_market_object(self):
        self.assertEqual(self.resp_data['name'], self.market.name)

    def test_includes_bikes(self):
        self.assertIsNotNone(self.resp_data['bike_set'])

    def test_includes_attribution_channels(self):
        self.assertTrue('attribution_channels' in self.resp_data)

    def test_includes_bike_image_set_list(self):
        self.assertTrue('bike_images' in self.resp_data)


class UnauthenticatedClosestMarketTestCase(TestCase):
    def setUp(self):
        self.market = f.MarketFactory()
        f.BikeFactory(market=self.market)
        self.resp = Client().get(
            '/markets/?lat=38.85&lon=-77.02',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)
        print(self.resp_data)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_includes_closest_bike(self):
        self.assertTrue('closest_bike' in self.resp_data['results'][0])

    def test_includes_closest_bike_distance(self):
        self.assertTrue(
            'closest_bike_distance' in self.resp_data['results'][0])


class UnauthenticatedListBikesTestCase(TestCase):
    def setUp(self):
        f.BikeFactory.create_batch(4)
        self.resp = Client().get(
            '/bikes/', content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_list_all_bikes(self):
        self.assertEqual(len(self.resp_data), 4)


class UnauthenticatedQueryBikesTestCase(TestCase):
    def setUp(self):
        f.BikeFactory(last_location=Point(-77.500, 38.500), status='reserved')
        f.BikeFactory(last_location=Point(-77.500, 38.502))
        f.BikeFactory(last_location=Point(-77.500, 38.498))
        f.BikeFactory(last_location=Point(-79.500, 40.500))
        f.BikeFactory(last_location=Point(-105.500, 6.500))

    def test_query_bikes_lat_lon_query(self):
        resp = Client().get(
            '/bikes/',
            content_type='application/json',
            data={'last_lat': 38.5, 'last_lon': -77.5})
        resp_data = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp_data['results']), 3)

    def test_query_bikes_lat_lon_radius_query(self):
        resp = Client().get(
            '/bikes/',
            content_type='application/json',
            data={'last_lat': 38.5, 'last_lon': -77.5, 'radius': 500})
        resp_data = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp_data['results']), 4)

    def test_query_bikes_by_status(self):
        resp = Client().get(
            '/bikes/',
            content_type='application/json',
            data={'status': 'available'})
        resp_data = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp_data['results']), 4)


class CreateBikeTestCase(AuthenticatedTestCase):
    def setUp(self):
        # set is_staff status to bike create permission
        super(CreateBikeTestCase, self).setUp(is_staff=True)
        self.bike = f.BikeFactory.build()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/bikes/',
            content_type='application/json',
            data=json.dumps({
                'name': self.bike.name,
                'last_location': str(self.bike.last_location),
                'launch_date': str(datetime.now())}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)


class BikeDetailTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(BikeDetailTestCase, self).setUp()
        self.bike = f.BikeFactory(owner=self.person, ambassador=self.person)
        self.detail1 = f.BikeDetailFactory(bike=self.bike)
        self.detail2 = f.BikeDetailPriceWithLockFactory(bike=self.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/bikes/%s/' % str(self.bike.guid))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_includes_name(self):
        self.assertEqual(self.resp_data['name'], self.bike.name)

    def test_includes_owner(self):
        self.assertEqual(self.resp_data['owner'], str(self.person.guid))

    def test_includes_ambassador(self):
        self.assertEqual(self.resp_data['ambassador'], str(self.person.guid))

    def test_includes_brand(self):
        self.assertEqual(self.resp_data['brand']['name'], self.bike.brand.name)

    def test_includes_style(self):
        self.assertEqual(self.resp_data['style']['name'], self.bike.style.name)

    def test_includes_size(self):
        self.assertEqual(self.resp_data['size']['name'], self.bike.size.name)

    def test_includes_status(self):
        self.assertEqual(self.resp_data['status'], self.bike.status)

    def test_includes_owner_available(self):
        self.assertEqual(
            self.resp_data['owner_available'], self.bike.owner_available)

    def test_includes_baas_available(self):
        self.assertEqual(
            self.resp_data['baas_available'], self.bike.baas_available)

    def test_includes_launch_date(self):
        launch_str = self.bike.launch_date.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        self.assertEqual(self.resp_data['launch_date'], launch_str)

    def test_includes_last_location(self):
        self.assertEqual(
            self.resp_data['last_location'], str(self.bike.last_location))

    def test_includes_last_lat(self):
        self.assertEqual(self.resp_data['last_lat'], self.bike.last_latitude())

    def test_includes_last_lon(self):
        self.assertEqual(
            self.resp_data['last_lon'], self.bike.last_longitude())

    def test_includes_details(self):
        self.assertIsNotNone(self.resp_data['detail_set'])

    def test_includes_details_price_with_lock(self):
        self.assertEqual(self.resp_data['detail_set']['price_with_lock'],
                         self.bike.details()['price_with_lock'])

    def test_includes_lock_info(self):
        self.assertIsNotNone(self.resp_data['lock'])

    def test_includes_baas_lock_id(self):
        self.assertEqual(
            self.resp_data['lock']['baas_lock_id'],
            self.bike.lock.baas_lock_id)

    def test_includes_battery_level_of_lock(self):
        self.assertEqual(
            self.resp_data['lock']['battery_remaining'],
            self.bike.lock.battery_remaining)

    def test_includes_type_of_lock(self):
        self.assertEqual(
            self.resp_data['lock']['type'],
            self.bike.lock.type)

    def test_includes_lock_urn(self):
        self.assertEqual(
            self.resp_data['lock']['urn'],
            self.bike.lock.urn())

    def test_includes_unique_renter_count(self):
        self.assertEqual(
            self.resp_data['unique_renter_count'],
            self.bike.unique_renter_count())

    def test_excludes_image_set(self):
        self.assertEqual(type(self.resp_data['image_set']), str)


class MarketContainsPointTestCase(TestCase):
    def setUp(self):
        self.market = f.MarketFactory()
        self.resp = Client().get(
            '/markets/%s/contains/?lat=38.85&lon=-77.02' %
            str(self.market.guid),
            content_type='application/json')
        print(self.resp.content)
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_point_is_in_market(self):
        self.assertTrue(self.resp_data['within_market'])


class MarketDoesNotContainPointTestCase(TestCase):
    def setUp(self):
        self.market = f.MarketFactory()
        self.resp = Client().get(
            '/markets/%s/contains/?lat=48.90&lon=-97.02' %
            str(self.market.guid),
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_point_is_not_in_market(self):
        self.assertFalse(self.resp_data['within_market'])


class CreateReservationTestCase(AuthenticatedTestCase):
    @patch('baas.models.payment_gateway.list_payment_methods')
    def setUp(self, fake_list_payment_methods):
        super(CreateReservationTestCase, self).setUp()
        fake_list_payment_methods.return_value = [1]
        self.bike = f.BikeFactory()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/reservations/',
            content_type='application/json',
            data=json.dumps({
                'bike': str(self.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 38.90)'}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['market'])

    def test_creates_reservation_record(self):
        self.assertIsNotNone(
            models.Reservation.objects.get(renter=self.person, bike=self.bike))


class CreateReservationWithoutPaymentMethodsTestCase(AuthenticatedTestCase):
    @patch('baas.models.payment_gateway.list_payment_methods')
    def setUp(self, fake_list_payment_methods):
        super(CreateReservationWithoutPaymentMethodsTestCase, self).setUp()
        fake_list_payment_methods.return_value = []
        self.bike = f.BikeFactory()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/reservations/',
            content_type='application/json',
            data=json.dumps({
                'bike': str(self.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 38.90)'}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_402(self):
        self.assertEqual(self.resp.status_code, 402)


class CancelReservationTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(CancelReservationTestCase, self).setUp()
        self.reservation = f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/reservations/%s/' % str(self.reservation.guid),
            content_type='application/json',
            data=json.dumps({'canceled': True}))
        self.resp_data = json.loads(self.resp.content)
        self.updated_reservation = models.Reservation.objects.get(
            guid=self.reservation.guid)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_sets_canceled_at(self):
        self.assertIsNotNone(self.resp_data['canceled_at'])
        self.assertIsNotNone(self.updated_reservation.canceled_at)


class GetActiveReservationsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(GetActiveReservationsTestCase, self).setUp()
        f.ExpiredReservationFactory(renter=self.person)
        f.CanceledReservationFactory(renter=self.person)
        f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/reservations/?status=active',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_active_reservations(self):
        date_fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['canceled_at'])
        self.assertTrue(
            datetime.strptime(results[0]['expires_at'], date_fmt)
            > datetime.now())


class GetCanceledReservationsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(GetCanceledReservationsTestCase, self).setUp()
        f.ExpiredReservationFactory(renter=self.person)
        f.CanceledReservationFactory(renter=self.person)
        f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/reservations/?status=canceled',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_canceled_reservations(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNotNone(results[0]['canceled_at'])


class GetExpiredReservationsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(GetExpiredReservationsTestCase, self).setUp()
        f.ExpiredReservationFactory(renter=self.person)
        f.CanceledReservationFactory(renter=self.person)
        f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/reservations/?status=expired',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_expired_reservations(self):
        date_fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['canceled_at'])
        self.assertTrue(
            datetime.strptime(results[0]['expires_at'], date_fmt)
            < datetime.now())


class NestedGetActiveReservationsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(NestedGetActiveReservationsTestCase, self).setUp()
        f.ExpiredReservationFactory(renter=self.person)
        f.CanceledReservationFactory(renter=self.person)
        f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/people/%s/reservations/?status=active' % self.person.guid,
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_active_reservations(self):
        date_fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['canceled_at'])
        self.assertTrue(
            datetime.strptime(results[0]['expires_at'], date_fmt)
            > datetime.now())


class NestedGetCanceledReservationsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(NestedGetCanceledReservationsTestCase, self).setUp()
        f.ExpiredReservationFactory(renter=self.person)
        f.CanceledReservationFactory(renter=self.person)
        f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/people/%s/reservations/?status=canceled' % self.person.guid,
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_canceled_reservations(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNotNone(results[0]['canceled_at'])


class NestedGetExpiredReservationsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(NestedGetExpiredReservationsTestCase, self).setUp()
        f.ExpiredReservationFactory(renter=self.person)
        f.CanceledReservationFactory(renter=self.person)
        f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/people/%s/reservations/?status=expired' % self.person.guid,
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        print(self.resp_data)
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_expired_reservations(self):
        date_fmt = '%Y-%m-%dT%H:%M:%S.%fZ'
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['canceled_at'])
        self.assertTrue(
            datetime.strptime(results[0]['expires_at'], date_fmt)
            < datetime.now())


class UnauthenticatedCreateReservationTestCase(TestCase):
    def setUp(self):
        self.bike = f.BikeFactory()
        self.resp = Client().post(
            '/reservations/',
            content_type='application/json',
            data=json.dumps({
                'bike': str(self.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 38.90)'}))

    def test_returns_403(self):
        self.assertEqual(self.resp.status_code, 403)


class GetActiveSessionsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(GetActiveSessionsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/sessions/?status=active',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_active_sessions(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['ended_at'])

    def test_includes_trips(self):
        results = self.resp_data['results']
        self.assertIsNotNone(results[0]['trip_set'])


class GetEndedSessionsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(GetEndedSessionsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/sessions/?status=ended',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_returns_only_ended_sessions(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 0)


class NestedGetActiveSessionsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(NestedGetActiveSessionsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/people/%s/sessions/?status=active' % self.person.guid,
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_contains_market(self):
        self.assertIsNotNone(self.resp_data['results'][0]['market'])

    def test_returns_only_active_sessions(self):
        results = self.resp_data['results']
        print(results)
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['ended_at'])


class NestedGetEndedSessionsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(NestedGetEndedSessionsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/people/%s/sessions/?status=ended' % self.person.guid,
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_returns_only_ended_sessions(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 0)


class GetActiveTripsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(GetActiveTripsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/trips/?status=active',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_returns_only_active_trips(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['ended_event'])


class GetEndedTripsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(GetEndedTripsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        f.LockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/trips/?status=ended',
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_returns_only_ended_trips(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNotNone(results[0]['ended_event'])


class NestedGetActiveTripsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(NestedGetActiveTripsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/people/%s/trips/?status=active' % self.person.guid,
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_returns_only_active_trips(self):
        results = self.resp_data['results']
        self.assertEqual(len(results), 1)
        self.assertIsNone(results[0]['ended_event'])


class NestedGetEndedTripsTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(NestedGetEndedTripsTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(person=self.person, bike=reservation.bike)
        f.LockEventFactory(person=self.person, bike=reservation.bike)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/people/%s/trips/?status=ended' % self.person.guid,
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_returns_only_ended_trips(self):
        results = self.resp_data['results']
        print(self.resp_data)
        self.assertEqual(len(results), 1)
        self.assertIsNotNone(results[0]['ended_event'])


class UnlockWithValidReservationTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(UnlockWithValidReservationTestCase, self).setUp()
        self.reservation = f.ReservationFactory(renter=self.person)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/lock_events/',
            content_type='application/json',
            data=json.dumps({
                'event_type': 'unlocked',
                'bike': str(self.reservation.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 38.90)'}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_creates_lock_event_record(self):
        self.assertIsNotNone(models.LockEvent.objects.get(
            event_type='unlocked',
            bike=self.reservation.bike,
            person=self.reservation.renter))

    def test_creates_open_session_record(self):
        session = models.Session.objects.get(
            bike=self.reservation.bike, renter=self.reservation.renter)
        self.assertIsNotNone(session.started_event)
        self.assertIsNone(session.ended_event)

    def test_creates_open_trip_record(self):
        session = models.Session.objects.get(
            bike=self.reservation.bike, renter=self.reservation.renter)
        trip = session.trip_set.first()
        self.assertIsNotNone(trip.started_event)
        self.assertIsNone(trip.ended_event)

    def test_returns_trip(self):
        self.assertIsNotNone(self.resp_data['trip'])


class UnlockWithoutValidReservationTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(UnlockWithoutValidReservationTestCase, self).setUp()
        self.bike = f.BikeFactory()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/lock_events/',
            content_type='application/json',
            data=json.dumps({
                'event_type': 'unlocked',
                'bike': str(self.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 38.90)'}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_400(self):
        self.assertEqual(self.resp.status_code, 400)

    def test_responds_with_invalid_session_state_message(self):
        self.assertEqual(
            self.resp_data['non_field_errors'][0],
            'No session or reservation for user and bike.')


class LockWithValidTripTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(LockWithValidTripTestCase, self).setUp()
        self.reservation = f.ReservationFactory(renter=self.person)
        self.unlock_event = f.UnlockEventFactory(
            bike=self.reservation.bike, person=self.reservation.renter)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/lock_events/',
            content_type='application/json',
            data=json.dumps({
                'event_type': 'locked',
                'bike': str(self.reservation.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 38.90)'}))

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_creates_lock_event_record(self):
        self.assertIsNotNone(models.LockEvent.objects.get(
            event_type='locked',
            bike=self.reservation.bike,
            person=self.reservation.renter))

    def test_leaves_session_open(self):
        session = models.Session.objects.get(
            bike=self.reservation.bike, renter=self.reservation.renter)
        self.assertIsNotNone(session.started_event)
        self.assertIsNone(session.ended_event)

    def test_closes_trip_record(self):
        session = models.Session.objects.get(
            bike=self.reservation.bike, renter=self.reservation.renter)
        trip = session.trip_set.first()
        self.assertIsNotNone(trip.started_event)
        self.assertIsNotNone(trip.ended_event)


class LockWithoutValidTripTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(LockWithoutValidTripTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(bike=reservation.bike, person=reservation.renter)
        f.LockEventFactory(bike=reservation.bike, person=reservation.renter)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/lock_events/',
            content_type='application/json',
            data=json.dumps({
                'event_type': 'locked',
                'bike': str(reservation.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 38.90)'}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_400(self):
        self.assertEqual(self.resp.status_code, 400)

    def test_responds_with_invalid_trip_state_message(self):
        self.assertEqual(
            self.resp_data['non_field_errors'][0], 'No trip in progress.')


class LockOutsideMarketTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(LockOutsideMarketTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(bike=reservation.bike, person=reservation.renter)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/lock_events/',
            content_type='application/json',
            data=json.dumps({
                'event_type': 'locked',
                'bike': str(reservation.bike.guid),
                'location': 'SRID=4326;POINT (-77.02 58.90)'}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_responds_with_outside_market_attribute(self):
        self.assertEqual(self.resp_data['outside_market'], True)


class EndTripSessionWithBikeSoldTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(EndTripSessionWithBikeSoldTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.BikeDetailPriceWithLockFactory(bike=reservation.bike)
        f.UnlockEventFactory(bike=reservation.bike, person=reservation.renter)
        self.session = models.Session.objects.get(bike=reservation.bike, renter=self.person)
        self.trip = self.session.trip_set.first()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/trips/%s/' % str(self.trip.guid),
            content_type='application/json',
            data=json.dumps({
                'session': self.session.guid,
                'started_event': self.session.started_event.guid,
                'route': "-1.0,58.90,-77.02,100.0,%s" % self.session.started_event.time.isoformat(),
                'sold': True}))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_closes_trip(self):
        self.trip.refresh_from_db()
        self.assertIsNotNone(self.trip.ended_event)

    def test_associates_session_ended_at(self):
        self.session.refresh_from_db()
        self.assertIsNotNone(self.session.ended_at)

    def test_changes_bike_sold(self):
        self.assertEqual(self.session.bike.status, "sold")
        self.assertEqual(self.session.bike.owner, self.session.renter)


class EndSessionOutsideMarketTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(EndSessionOutsideMarketTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(bike=reservation.bike, person=reservation.renter)
        f.LockEventFactory(
            bike=reservation.bike,
            person=reservation.renter,
            location=Point(-77.02, 58.90))
        self.session = models.Session.objects.last()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/sessions/%s/' % str(self.session.guid),
            content_type='application/json',
            data=json.dumps({'ended': True}))

    def test_returns_402(self):
        self.assertEqual(self.resp.status_code, 402)

    def test_does_not_associate_session_ended_event(self):
        last_session = models.Session.objects.last()
        self.assertIsNone(last_session.ended_event)

    def test_does_not_associate_session_ended_at(self):
        last_session = models.Session.objects.last()
        self.assertIsNone(last_session.ended_at)


class ForceEndSessionOutsideMarketTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(ForceEndSessionOutsideMarketTestCase, self).setUp()
        reservation = f.ReservationFactory(renter=self.person)
        f.UnlockEventFactory(bike=reservation.bike, person=reservation.renter)
        f.LockEventFactory(
            bike=reservation.bike,
            person=reservation.renter,
            location=Point(-77.02, 58.90))
        self.session = models.Session.objects.last()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).patch(
            '/sessions/%s/' % str(self.session.guid),
            content_type='application/json',
            data=json.dumps({
                'ended': True,
                'end_outside_market': True}))

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_does_not_associate_session_ended_event(self):
        last_session = models.Session.objects.last()
        self.assertIsNone(last_session.ended_event)

    def test_associates_session_ended_at(self):
        last_session = models.Session.objects.last()
        self.assertIsNotNone(last_session.ended_at)


class QueryFeedbackPromptSetsTestCase(AuthenticatedTestCase):
    def setUp(self):
        set1 = f.FeedbackPromptSetFactory(name='session_end_feedback')
        set2 = f.FeedbackPromptSetFactory(name='out_of_session_feedback')
        set1.prompts.add(f.FeedbackPromptFactory())
        set1.prompts.add(f.FeedbackPromptFactory())
        set2.prompts.add(f.FeedbackPromptFactory())
        super(QueryFeedbackPromptSetsTestCase, self).setUp()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).get(
            '/feedback_prompt_sets/?name=%s' % (set1.name),
            content_type='application/json')
        self.resp_data = json.loads(self.resp.content)

    def test_returns_200(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_query_feedback_prompt_sets_by_set_name(self):
        self.assertEqual(len(self.resp_data['results'][0]['prompts']), 2)

    def test_embeds_feedback_prompts(self):
        first_prompt = self.resp_data['results'][0]['prompts'][0]
        self.assertIsNotNone(first_prompt['default'])


class CreateFeedbackEventTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(CreateFeedbackEventTestCase, self).setUp()
        bike = f.BikeFactory()
        self.prompt_set = f.FeedbackPromptSetFactory()
        prompts = f.FeedbackPromptFactory.create_batch(3)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/feedback_events/',
            content_type='application/json',
            data=json.dumps({
                'bike': str(bike.guid),
                'prompts': str(self.prompt_set.guid),
                'star_rating': 4,
                'detail_set': [
                    {'prompt': str(prompts[0].guid), 'value': 'True'},
                    {'prompt': str(prompts[1].guid), 'value': 'False'},
                    {'prompt': str(prompts[2].guid), 'value': 'Missing Wheel'}
                ]
            }))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_201(self):
        self.assertTrue(self.resp.status_code, 201)

    def test_creates_feedback_event_record(self):
        self.assertEqual(models.FeedbackEvent.objects.all().count(), 1)

    def test_defaults_bike_rideable_to_true(self):
        feedback_event = models.FeedbackEvent.objects.last()
        self.assertEqual(feedback_event.bike_rideable, True)

    def test_new_feedback_event_record_has_prompt_set(self):
        feedback_event = models.FeedbackEvent.objects.last()
        self.assertEqual(
            feedback_event.prompts.guid, str(self.prompt_set.guid))

    def test_new_feedback_event_record_has_detail_set(self):
        feedback_event = models.FeedbackEvent.objects.last()
        self.assertEqual(feedback_event.detail_set.count(), 3)


class CreateFeedbackEventWithNoStarRatingTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(CreateFeedbackEventWithNoStarRatingTestCase, self).setUp()
        bike = f.BikeFactory()
        self.prompt_set = f.FeedbackPromptSetFactory()
        prompts = f.FeedbackPromptFactory.create_batch(3)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/feedback_events/',
            content_type='application/json',
            data=json.dumps({
                'bike': str(bike.guid),
                'prompts': str(self.prompt_set.guid),
                'detail_set': [
                    {'prompt': str(prompts[0].guid), 'value': 'True'},
                    {'prompt': str(prompts[1].guid), 'value': 'False'},
                    {'prompt': str(prompts[2].guid), 'value': 'Missing Wheel'}
                ]
            }))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_201(self):
        self.assertTrue(self.resp.status_code, 201)

    def test_creates_feedback_event_record(self):
        self.assertEqual(models.FeedbackEvent.objects.all().count(), 1)

    def test_defaults_star_rating_to_negative_one(self):
        feedback_event = models.FeedbackEvent.objects.last()
        self.assertEqual(feedback_event.star_rating, -1)


class CreateFeedbackEventWhereBikeNotRideableTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(CreateFeedbackEventWhereBikeNotRideableTestCase, self).setUp()
        bike = f.BikeFactory()
        self.prompt_set = f.FeedbackPromptSetFactory()
        prompts = f.FeedbackPromptFactory.create_batch(3)
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/feedback_events/',
            content_type='application/json',
            data=json.dumps({
                'bike': str(bike.guid),
                'prompts': str(self.prompt_set.guid),
                'star_rating': 4,
                'bike_rideable': False,
                'detail_set': [
                    {'prompt': str(prompts[0].guid), 'value': 'True'},
                    {'prompt': str(prompts[1].guid), 'value': 'False'},
                    {'prompt': str(prompts[2].guid), 'value': 'Missing Wheel'}
                ]
            }))
        self.resp_data = json.loads(self.resp.content)

    def test_returns_201(self):
        self.assertTrue(self.resp.status_code, 201)

    def test_sets_bike_rideable_to_false(self):
        feedback_event = models.FeedbackEvent.objects.last()
        self.assertEqual(feedback_event.bike_rideable, False)


class CreatePaymentMethodTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(CreatePaymentMethodTestCase, self).setUp()
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/people/%s/payment_methods/' % str(self.person.guid),
            content_type='application/json',
            data=json.dumps({
                'nonce': 'abcdefg'
            }))

    def test_returns_201(self):
        self.assertEqual(self.resp.status_code, 201)


class AnswerCodeTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(AnswerCodeTestCase, self).setUp()
        challenge_code = 1000
        self.reservation = f.ReservationFactory(renter=self.person)
        self.bike = self.reservation.bike
        self.lock = self.bike.lock
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/answer_codes/',
            content_type='application/json',
            data=json.dumps({
                'lock': self.lock.urn(),
                'challenge_code': challenge_code
            }))
        self.resp_data = json.loads(self.resp.content)
        print(self.resp.content)

    def test_returns_200(self):
        self.assertTrue(self.resp.status_code, 201)

    def test_responds_answer_code(self):
        self.assertEqual(self.resp_data['answer_code'], 0)


class AnswerCodeInvalidAttemptTestCase(AuthenticatedTestCase):
    def setUp(self):
        super(AnswerCodeInvalidAttemptTestCase, self).setUp()
        challenge_code = 1000
        self.bike = f.BikeFactory()
        self.lock = self.bike.lock
        self.resp = Client(HTTP_AUTHORIZATION=self.token).post(
            '/answer_codes/',
            content_type='application/json',
            data=json.dumps({
                'lock': self.lock.urn(),
                'challenge_code': challenge_code
            }))
        self.resp_data = json.loads(self.resp.content)
        print(self.resp.content)

    def test_returns_403(self):
        self.assertTrue(self.resp.status_code, 403)

    def test_responds_with_error_message(self):
        self.assertEqual(
            self.resp_data['non_field_errors'], 'Invalid bike for user.')
