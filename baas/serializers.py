import base64
from django.contrib.auth import models as auth_models
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.password_validation import validate_password
from django.contrib.gis.geos import fromstr
from django.core import exceptions
from baas import models
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from rest_framework.exceptions import APIException

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger('baas_debug')


class PaymentRequiredException(APIException):
    status_code = 402
    default_detail = 'Cannot end session outside market.'


class UserSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    password = serializers.CharField(write_only=True)
    old_password = serializers.CharField(write_only=True, required=False)
    new_password = serializers.CharField(write_only=True, required=False)
    confirm_password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = get_user_model()
        fields = (
            'pk', 'email', 'groups', 'password', 'old_password',
            'new_password', 'confirm_password')

    def validate(self, data):
        """
        Check password fields requirement
        """
        if data.get('new_password'):
            if not data.get('old_password'):
                raise serializers.ValidationError({"old_password": "Field is required for password update"})
            if not data.get('confirm_password'):
                raise serializers.ValidationError({"confirm_password": "Field is required for password update"})
        return data

    def validate_password(self, value):
        user = self.context['request'].user
        validate_password(password=value, user=user)
        return value

    def validate_new_password(self, value):
        user = self.context['request'].user
        validate_password(password=value, user=user)
        return value


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = auth_models.Group
        fields = ('name',)


class PersonSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    user = UserSerializer(many=False, read_only=False)
    payment_gateway_id = serializers.CharField(read_only=True)
    token = serializers.CharField(read_only=True)
    self_reported_channel_code = serializers.CharField(
        write_only=True, required=False)
    auto_reported_channel_code = serializers.CharField(
        write_only=True, required=False)

    class Meta:
        model = models.Person
        fields = (
            'guid', 'first_name', 'last_name', 'phone_number',
            'payment_gateway_id', 'user', 'token', 'self_reported_channel_code',
            'auto_reported_channel_code')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def create(self, validated_data):
        user_data = validated_data['user']
        phone_number = ''
        if 'phone_number' in validated_data:
            phone_number = validated_data['phone_number']
        user = get_user_model().objects.create_user(
            email=user_data['email'],
            password=user_data['password'])
        person = models.Person.objects.create(
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            phone_number=phone_number,
            user=user)
        if 'self_reported_channel_code' in validated_data:
            code = validated_data['self_reported_channel_code']
            channel = models.AttributionChannel.objects.get(attribution_code=code)
            if channel:
                channel.self_reported_people.add(person)
                channel.save()
        if 'auto_reported_channel_code' in validated_data:
            code = validated_data['auto_reported_channel_code']
            channel = models.AttributionChannel.objects.get(attribution_code=code)
            if channel:
                channel.auto_reported_people.add(person)
                channel.save()
        return person

    def update(self, instance, validated_data):
        if 'first_name' in validated_data:
            instance.first_name = validated_data['first_name']
        if 'last_name' in validated_data:
            instance.last_name = validated_data['last_name']
        if 'phone_number' in validated_data:
            instance.phone_number = validated_data['phone_number']
        if 'user' in validated_data:
            user_data = validated_data['user']
            if 'email' in user_data:
                instance.user.email = user_data['email']
                instance.user.username = user_data['email']
            if all(k in user_data for k in (
               'old_password', 'new_password', 'confirm_password')):
                auth_check_user = authenticate(
                    email=instance.user.email,
                    password=user_data['old_password'])
                new_password = user_data['new_password']
                confirm_password = user_data['confirm_password']
                if (instance.user != auth_check_user):
                    raise serializers.ValidationError(
                        {'user': {'old_password': 'Old password is incorrect'}})
                elif (new_password != confirm_password):
                    raise serializers.ValidationError(
                        {'user': {'confirm_password': 'Passwords do not match'}})
                else:
                    instance.user.set_password(new_password)
            instance.user.save()
        instance.save()
        payload = api_settings.JWT_PAYLOAD_HANDLER(instance.user)
        instance.token = api_settings.JWT_ENCODE_HANDLER(payload)
        return instance


class LockSerializer(serializers.ModelSerializer):
    urn = serializers.CharField(read_only=True)

    class Meta:
        model = models.Lock
        fields = ('guid', 'type', 'baas_lock_id', 'urn', 'battery_remaining',)


class BikeBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BikeBrand
        fields = ('name',)


class BikeDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BikeDetail
        list_serializer_class = serializers.ListSerializer
        fields = ('bike', 'name', 'value')


class BikeSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BikeSize
        fields = ('name',)


class BikeStyleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BikeStyle
        fields = ('name',)


class BikeSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    last_lat = serializers.FloatField(source='last_latitude', read_only=True)
    last_lon = serializers.FloatField(source='last_longitude', read_only=True)
    launch_date = serializers.DateTimeField(format='%Y-%m-%dT%H:%M:%S.%fZ')
    image_file = serializers.FileField(write_only=True, required=False)
    brand = BikeBrandSerializer(read_only=True)
    size = BikeSizeSerializer(read_only=True)
    style = BikeStyleSerializer(read_only=True)
    detail_set = serializers.DictField(source='details', read_only=True)
    lock = LockSerializer(read_only=True)
    unique_renter_count = serializers.IntegerField(read_only=True)
    sell_price = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.Bike
        fields = (
            'guid', 'created_at', 'deleted', 'name', 'owner', 'ambassador',
            'brand', 'style', 'size', 'status', 'owner_available',
            'baas_available', 'launch_date', 'current_renter', 'last_location',
            'last_lat', 'last_lon', 'image_file', 'detail_set', 'lock',
            'unique_renter_count', 'image_set', 'listing_type', 'sell_price')


class AttributionChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AttributionChannel
        fields = ('guid', 'name', 'descriptive_phrase', 'attribution_code')


class MarketSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    bike_set = BikeSerializer(read_only=True, many=True)
    attribution_channels = AttributionChannelSerializer(
        read_only=True, many=True)
    bike_images = serializers.SerializerMethodField()
    closest_bike = serializers.SerializerMethodField()
    closest_bike_distance = serializers.SerializerMethodField()

    class Meta:
        model = models.Market
        fields = ('guid', 'name', 'geoshape', 'bike_set',
                  'attribution_channels', 'bike_images', 'closest_bike',
                  'closest_bike_distance')

    def get_bike_images(self, obj):
        result_dict = {}
        for image_set in obj.bike_image_sets.distinct():
            image_set_dict = {}
            for bikeimage in image_set.bikeimage_set.all():
                image_set_dict[bikeimage.key] = bikeimage.image_uri
            result_dict[image_set.guid] = image_set_dict
        return result_dict

    def get_closest_bike(self, obj):
        lat = self.context['request'].query_params.get('lat', None)
        lon = self.context['request'].query_params.get('lon', None)
        if lat and lon and obj.bike_set.count() > 0:
            lat_lon_str = 'POINT(%s %s)' % (lon, lat)
            lat_lon     = fromstr(lat_lon_str, srid=4326)
            return BikeSerializer(obj.get_closest_bike(lat_lon)).data
        else:
            return None

    def get_closest_bike_distance(self, obj):
        lat = self.context['request'].query_params.get('lat', None)
        lon = self.context['request'].query_params.get('lon', None)
        if lat and lon and obj.bike_set.count() > 0:
            lat_lon_str = 'POINT(%s %s)' % (lon, lat)
            lat_lon     = fromstr(lat_lon_str, srid=4326)
            return obj.get_closest_bike_distance(lat_lon)
        else:
            return None


class MarketSummarySerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)

    class Meta:
        model = models.Market
        fields = ('guid', 'name', 'geoshape')


class MarketContentsSerializer(serializers.Serializer):
    within_market = serializers.BooleanField(read_only=True)

    class Meta:
        fields = ('within_market')


class ReservationSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    renter = serializers.CharField(read_only=True)
    market = MarketSummarySerializer(read_only=True)
    bike_object = BikeSerializer(read_only=True, source='bike')
    started_at = serializers.DateTimeField(
        read_only=True, format='%Y-%m-%dT%H:%M:%S.%fZ')
    expires_at = serializers.DateTimeField(
        read_only=True, format='%Y-%m-%dT%H:%M:%S.%fZ')
    canceled_at = serializers.DateTimeField(
        read_only=True, format='%Y-%m-%dT%H:%M:%S.%fZ')
    canceled = serializers.BooleanField(write_only=True, required=False)
    session_started_at = serializers.DateTimeField(
        read_only=True, format='%Y-%m-%dT%H:%M:%S.%fZ')

    class Meta:
        model = models.Reservation
        fields = (
            'guid', 'bike', 'bike_object', 'renter', 'location', 'started_at',
            'expires_at', 'canceled_at', 'session_started_at', 'canceled',
            'market')

    def create(self, validated_data):
        renter = models.Person.objects.get(user=self.context['request'].user)
        try:
            return models.Reservation.objects.create(
                bike=validated_data['bike'],
                renter=renter,
                location=validated_data['location'])
        except exceptions.ValidationError as e:
            logger.error(e)
            raise serializers.ValidationError(e.message_dict)

    def update(self, instance, validated_data):
        if validated_data['canceled'] is True and instance.canceled_at is None:
            instance.cancel()
        try:
            instance.save()
        except exceptions.ValidationError as e:
            logger.error(e)
            raise serializers.ValidationError(e.message_dict)
        return instance


class TripSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    sold = serializers.BooleanField(write_only=True, required=False)

    class Meta:
        model = models.Trip
        fields = ('guid', 'session', 'started_event', 'ended_event', 'route', 'sold')

    def update(self, instance, validated_data):
        if 'route' in validated_data:
            instance.route = validated_data['route']
            instance.update_bike_position_from_route()

        try:
            instance.save()

            # handle purchase with session close
            if validated_data.get('sold'):
                # close trip with generated lock event
                if not instance.ended_event:
                    instance.ended_event = models.LockEvent.objects.create(
                        event_type='locked',
                        bike=instance.started_event.bike,
                        person=instance.started_event.person,
                        location=instance.session.bike.last_location
                    )
                session = instance.session
                session.end_by_purchase = True
                session.end_session_by_purchase()
        except exceptions.ValidationError as e:
            logger.error(e)
            raise serializers.ValidationError(e.message_dict)
        return instance


class SessionSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    bike = BikeSerializer(read_only=True)
    market = MarketSummarySerializer(read_only=True)
    reservation = ReservationSerializer(read_only=True)
    ended = serializers.BooleanField(write_only=True, required=False)
    started_at = serializers.DateTimeField(format='%Y-%m-%dT%H:%M:%S.%fZ')
    ended_at = serializers.DateTimeField(format='%Y-%m-%dT%H:%M:%S.%fZ')
    end_outside_market = serializers.BooleanField(required=False)
    end_by_purchase = serializers.BooleanField(required=False)
    trip_set = TripSerializer(read_only=True, many=True)

    class Meta:
        model = models.Session
        fields = (
            'guid', 'bike', 'market', 'renter', 'owner', 'ambassador',
            'reservation', 'started_event', 'started_at', 'ended_event',
            'ended_at', 'ended', 'end_outside_market', 'end_by_purchase', 'trip_set')
        depth = 1

    def update(self, instance, validated_data):
        if 'end_outside_market' in validated_data:
            instance.end_outside_market = validated_data['end_outside_market']
        if 'end_by_purchase' in validated_data:
            instance.end_by_purchase = validated_data['end_by_purchase']
        if validated_data['ended'] is True and instance.ended_at is None:
            if instance.end_by_purchase:
                try:
                    instance.end_session_by_purchase()
                except exceptions.ValidationError as e:
                    logger.error(e)
                    raise serializers.ValidationError(e.message_dict)
            else:
                try:
                    instance.end_session()
                except models.BikeOutsideMarketException as e:
                    raise PaymentRequiredException(e)
        instance.save()
        return instance


class LockEventSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    time = serializers.DateTimeField(
        read_only=True, format='%Y-%m-%dT%H:%M:%S.%fZ')
    person = serializers.CharField(read_only=True)
    associated_session = serializers.CharField(
        source='associated_session_guid', read_only=True)
    outside_market = serializers.BooleanField(read_only=True)
    trip = TripSerializer(source='get_trip', read_only=True)

    class Meta:
        model = models.LockEvent
        fields = (
            'guid', 'event_type', 'bike', 'person', 'time', 'location',
            'associated_session', 'outside_market', 'trip')

    def create(self, validated_data):
        person = models.Person.objects.get(user=self.context['request'].user)
        try:
            instance = models.LockEvent.objects.create(
                event_type=validated_data['event_type'],
                bike=validated_data['bike'],
                person=person, location=validated_data['location'])
            return instance
        except exceptions.ValidationError as e:
            logger.error(e)
            raise serializers.ValidationError(e.message_dict)


class FeedbackPromptSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)

    class Meta:
        model = models.FeedbackPrompt
        fields = ('guid', 'default')


class FeedbackPromptSetSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    prompts = serializers.SerializerMethodField()

    class Meta:
        model = models.FeedbackPromptSet
        fields = ('guid', 'name', 'prompts')
        depth = 1

    def get_prompts(self, obj):
        prompts = obj.prompts.order_by('sort_index')
        serializer = FeedbackPromptSerializer(
            prompts, many=True, read_only=True)
        return serializer.data


class FeedbackEventDetailSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)

    class Meta:
        model = models.FeedbackEventDetail
        fields = ('guid', 'prompt', 'value')


class FeedbackEventSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)
    reporter = PersonSerializer(read_only=True)
    time = serializers.DateTimeField(
        read_only=True, format='%Y-%m-%dT%H:%M:%S.%fZ')
    detail_set = FeedbackEventDetailSerializer(many=True)
    star_rating = serializers.IntegerField(required=False)

    class Meta:
        model = models.FeedbackEvent
        fields = (
            'guid', 'bike', 'prompts', 'reporter', 'time', 'location',
            'bike_rideable', 'star_rating', 'detail_set')

    def create(self, validated_data):
        reporter = models.Person.objects.get(
            user=self.context['request'].user)
        event = models.FeedbackEvent.objects.create(
            reporter=reporter,
            bike=validated_data.get('bike'),
            prompts=validated_data.get('prompts'))
        if 'star_rating' in validated_data:
            event.star_rating = validated_data.get('star_rating')
        if 'bike_rideable' in validated_data:
            event.bike_rideable = validated_data.get('bike_rideable')
        event.save()
        for item in validated_data.get('detail_set'):
            models.FeedbackEventDetail.objects.create(event=event, **item)
        return event


class PaymentMethodSerializer(serializers.Serializer):
    card_type = serializers.CharField(read_only=True)
    issuing_bank = serializers.CharField(read_only=True)
    default = serializers.BooleanField(read_only=True)
    expired = serializers.BooleanField(read_only=True)
    last_4 = serializers.CharField(read_only=True)
    expiration_month = serializers.CharField(read_only=True)
    expiration_year = serializers.CharField(read_only=True)

    class Meta:
        list_serializer_class = serializers.ListSerializer
        fields = (
            'card_type', 'issuing_bank', 'default', 'expired', 'last_4',
            'expiration_month', 'expiration_year')


class AnswerCodeSerializer(serializers.Serializer):
    answer_code = serializers.IntegerField(read_only=True)

    class Meta:
        fields = ('answer_code')
