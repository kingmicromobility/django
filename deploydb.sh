if python manage.py test
then
  source venv/bin/activate
  python manage.py makemigrations
  git add --all

  echo "Enter a commit message (optional):"
  read -e USER_MESSAGE

  MESSAGE="deploy"

  if [ "$USER_MESSAGE" != "" ]
  then
  MESSAGE=$USER_MESSAGE
  fi

  git commit -am "$MESSAGE"

  # Send notification to slack.
  sha=$(git log --pretty=format:'%H' -n 1)
  author=$(git log --pretty=format:'%an' -n 1)
  comment=$(git log --pretty=format:'%s' -n 1)
  diff_url="https://bitbucket.org/baasproject/django/commits/"
  message="*Staging deploy with database migrations started by $author*:\n_"$comment"_\n<$diff_url$sha>"
  channel="#api_updates"
  username="deploybot"
  icon_emoji=":rocket:"
  curl -X POST --data '{"channel": "'"$channel"'", "username": "'"$username"'", "text": "'"$message"'", "icon_emoji": "'"$icon_emoji"'"}' https://hooks.slack.com/services/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

  git push origin master
  git push heroku master
  heroku run python manage.py migrate --app baasbikes-staging

  # Send notification to slack.
  message="*Staging deploy with database migrations complete!*"
  channel="#api_updates"
  username="deploybot"
  icon_emoji=":rocket:"
  curl -X POST --data '{"channel": "'"$channel"'", "username": "'"$username"'", "text": "'"$message"'", "icon_emoji": "'"$icon_emoji"'"}' https://hooks.slack.com/services/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
else
  echo "Test suite failed, preventing deploy."
fi
