# Baas Bikes Core API
This project contains the Baas Bikes core api.

## Development Environment Setup
_*NOTE:* These steps are incomplete_

1.  Install system requirements
    ```shell
    $ brew install redis
    ```
1.  Install python requirements
    ```shell
    $ source a
    $ pip install -r requirements.txt --allow-all-external
    ```
1. Copy `.env.example` to `.env`, and fill out missing values.
1.  Generate static files
    ```shell
    $ python manage.py collectstatic
    ```


## Tests
The automated test suite for this project contains unit tests for models, and
integration tests for API endpoints. You can run the test suite using:
```shell
$ python manage.py test
```

To test the appearance of email messages, use mailcatcher. Mailcatcher is a
lightweight SMTP server that runs locally, capturing messages sent by django,
and allowing you to view them in a web browser. First install it
with:
```shell
$ sudo gem install mailcatcher
```
Then fire it up with:
```shell
$ mailcatcher
```

## Deploying to Heroku
If your changes include database migrations, run:
```shell
$ sh deploydb.sh
```
This will push your changes to Heroku and immediately run database migrations.

Otherwise, just run:
```shell
$ sh deploy.sh
```

## Syntax Checking on Atom with Flake8

1.  Install flake8 (Python's linter package)
    ```shell
    $ pip install flake8
    $ pip install flake8-docstrings
    ```

1.  Install Atom's linter package, and the flake8 adapter
    ```shell
    $ apm install linter
    $ apm install linter-flake8
    ```

1.  Restart Atom. You'll probably see an error like this:
    ```
    The linter binary flake8 cannot be found
    ```
    Fix this error by adding the following line to your Atom init script
    (Atom -> Open your Init Script):
    ```coffeescript
    process.env.PATH = ['/usr/local/bin/', process.env.PATH].join(':')
    ```

1.  Restart Atom again. Things should work now! To confirm, write some broken
    Python code and save the file. Errors & warnings will appear inline and in
    a pane at the bottom of Atom
