import operator
from functools import reduce
from baas import models
from collections import OrderedDict
from datetime import datetime, timedelta
from django.db.models import Q

default_time_ranges = {
    'daily': timedelta(days=30),
    'weekly': timedelta(weeks=13),
    'monthly': timedelta(days=365)
}


class SessionReport(object):
    def __init__(self, **kwargs):
        self.resultset = []
        self.resolution = kwargs.get('resolution', 'weekly')
        self.market = kwargs.get('market', None)
        self.person = kwargs.get('person', None)
        self.bike = kwargs.get('bike', None)
        self.exclude_non_payers = kwargs.get('exclude_non_payers', True)
        self.originating_in = kwargs.get('originating_in', None)
        self.terminating_in = kwargs.get('terminating_in', None)
        self.intersecting = kwargs.get('intersecting', None)
        self.starting_before = kwargs.get('starting_before', datetime.now())
        self.starting_after = kwargs.get(
            'starting_after',
            self.starting_before - default_time_ranges[self.resolution])

    def get_queryset(self):
        query = [
            Q(started_at__lte=self.starting_before),
            Q(started_at__gte=self.starting_after)]
        if self.market is not None:
            query.append(Q(bike__market=self.market))
        if self.person is not None:
            query.append(Q(renter=self.person))
        if self.bike is not None:
            query.append(Q(bike=self.bike))
        if self.exclude_non_payers:
            query.append(Q(renter__paying_customer=True))
        if self.originating_in is not None:
            query.append(Q(started_event__location__within=self.originating_in))

        return models.Session.objects\
            .select_related('renter')\
            .exclude(ended_at=None)\
            .filter(reduce(operator.and_, query))\
            .order_by('ended_at')

    def get_resultset(self):
        self.resultset = self.get_queryset().all()

    def duration_distribution(self, range_begin=0, range_end=18000, step=300):
        if len(self.resultset) < 1:
            self.get_resultset()

        data = OrderedDict()
        for lower_bound in range(range_begin, range_end + step, step):
            data[lower_bound] = 0
        for session in self.resultset:
            duration = (session.ended_at - session.started_at).seconds
            key = min(duration / step * step, range_end)
            data[key] += 1
        return data
