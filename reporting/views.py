import datetime
import calendar
import operator
from functools import reduce
from baas import models, date_util
from reporting import models as reports
from reporting import mixpanel
from collections import OrderedDict
from django.db.models import Q
from rest_framework import viewsets, status
from rest_framework.response import Response


class NewSignupCountReportViewSet(viewsets.ViewSet):
    def generate_query(self, request):
        default_ranges = {
            'daily': datetime.timedelta(days=30),
            'weekly': datetime.timedelta(weeks=13),
            'monthly': datetime.timedelta(days=365)
        }
        resolution = self.request.query_params.get('resolution', 'weekly')
        end = self.request.query_params.get('end', datetime.datetime.now())
        begin = self.request.query_params.get(
            'begin', end - default_ranges[resolution])
        excl_non_paying = self.request.query_params.get(
            'exclude_non_payers', True) == 'True'

        query = [Q(created_at__lte=end), Q(created_at__gte=begin)]
        if excl_non_paying:
            query.append(Q(paying_customer=True))

        return models.Person.objects\
            .filter(reduce(operator.and_, query))\
            .order_by('created_at')\
            .all()

    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        people = self.generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for person in people:
            key = str(person.created_at.date())
            if resolution == 'monthly':
                key = calendar.month_name[person.created_at.month]
            elif resolution == 'weekly':
                key = person.created_at.isocalendar()[1]
            if key in data:
                data[key] += 1
            else:
                data[key] = 1

        # calculate total for each resolution unit ---
        report = OrderedDict(sorted(data.items()))

        return Response(report, status=status.HTTP_200_OK)


class NewRiderCountReportViewSet(viewsets.ViewSet):
    def generate_query(self, request):
        default_ranges = {
            'daily': datetime.timedelta(days=30),
            'weekly': datetime.timedelta(weeks=13),
            'monthly': datetime.timedelta(days=365)
        }
        resolution = self.request.query_params.get('resolution', 'weekly')
        end = self.request.query_params.get('end', datetime.datetime.now())
        begin = self.request.query_params.get(
            'begin', end - default_ranges[resolution])
        excl_non_paying = self.request.query_params.get(
            'exclude_non_payers', True) == 'True'

        query = [Q(created_at__lte=end), Q(created_at__gte=begin)]
        if excl_non_paying:
            query.append(Q(paying_customer=True))

        return models.Person.objects\
            .filter(reduce(operator.and_, query))\
            .order_by('created_at')\
            .all()

    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        people = self.generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for person in people:
            if person.is_active():
                first_session = person.first_significant_session()
                key = str(first_session.ended_at.date())
                if resolution == 'monthly':
                    key = calendar.month_name[first_session.ended_at.month]
                elif resolution == 'weekly':
                    key = first_session.ended_at.isocalendar()[1]
                if key in data:
                    data[key] += 1
                else:
                    data[key] = 1

        # calculate total for each resolution unit ---
        report = OrderedDict(sorted(data.items()))

        return Response(report, status=status.HTTP_200_OK)


class NewRepeatRiderCountReportViewSet(viewsets.ViewSet):
    def generate_query(self, request):
        default_ranges = {
            'daily': datetime.timedelta(days=30),
            'weekly': datetime.timedelta(weeks=13),
            'monthly': datetime.timedelta(days=365)
        }
        resolution = self.request.query_params.get('resolution', 'weekly')
        end = self.request.query_params.get('end', datetime.datetime.now())
        begin = self.request.query_params.get(
            'begin', end - default_ranges[resolution])
        excl_non_paying = self.request.query_params.get(
            'exclude_non_payers', True) == 'True'

        query = [Q(created_at__lte=end), Q(created_at__gte=begin)]
        if excl_non_paying:
            query.append(Q(paying_customer=True))

        return models.Person.objects\
            .filter(reduce(operator.and_, query))\
            .order_by('created_at')\
            .all()

    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        people = self.generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for person in people:
            if person.is_repeat():
                repeat_session = person.first_repeat_session()
                key = str(repeat_session.ended_at.date())
                if resolution == 'monthly':
                    key = calendar.month_name[repeat_session.ended_at.month]
                elif resolution == 'weekly':
                    key = repeat_session.ended_at.isocalendar()[1]
                if key in data:
                    data[key] += 1
                else:
                    data[key] = 1

        # calculate total for each resolution unit ---
        report = OrderedDict(sorted(data.items()))

        return Response(report, status=status.HTTP_200_OK)


class TimeToFirstRideReportViewSet(viewsets.ViewSet):
    def generate_query(self, request):
        default_ranges = {
            'daily': datetime.timedelta(days=30),
            'weekly': datetime.timedelta(weeks=13),
            'monthly': datetime.timedelta(days=365)
        }
        resolution = self.request.query_params.get('resolution', 'weekly')
        end = self.request.query_params.get('end', datetime.datetime.now())
        begin = self.request.query_params.get(
            'begin', end - default_ranges[resolution])
        excl_non_paying = self.request.query_params.get(
            'exclude_non_payers', True) == 'True'

        query = [Q(created_at__lte=end), Q(created_at__gte=begin)]
        if excl_non_paying:
            query.append(Q(paying_customer=True))

        return models.Person.objects\
            .filter(reduce(operator.and_, query))\
            .order_by('created_at')\
            .all()

    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        people = self.generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for person in people:
            if person.is_active():
                first_session = person.first_significant_session()
                diff = first_session.started_at - person.created_at
                key = str(person.created_at.date())
                if resolution == 'monthly':
                    key = calendar.month_name[person.created_at.month]
                elif resolution == 'weekly':
                    key = person.created_at.isocalendar()[1]
                if key in data:
                    data[key].append(diff.seconds / 3600.0)
                else:
                    data[key] = [diff.seconds / 3600.0]

        # calculate total for each resolution unit ---
        report = OrderedDict(sorted(data.items()))
        for date in report.keys():
            if len(report[date]) > 0:
                report[date] = sum(report[date]) / float(len(report[date]))
            else:
                report[date] = 0.0

        return Response(report, status=status.HTTP_200_OK)


class TimeToRepeatRideReportViewSet(viewsets.ViewSet):
    def generate_query(self, request):
        default_ranges = {
            'daily': datetime.timedelta(days=30),
            'weekly': datetime.timedelta(weeks=13),
            'monthly': datetime.timedelta(days=365)
        }
        resolution = self.request.query_params.get('resolution', 'weekly')
        end = self.request.query_params.get('end', datetime.datetime.now())
        begin = self.request.query_params.get(
            'begin', end - default_ranges[resolution])
        excl_non_paying = self.request.query_params.get(
            'exclude_non_payers', True) == 'True'

        query = [Q(created_at__lte=end), Q(created_at__gte=begin)]
        if excl_non_paying:
            query.append(Q(paying_customer=True))

        return models.Person.objects\
            .filter(reduce(operator.and_, query))\
            .order_by('created_at')\
            .all()

    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        people = self.generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for person in people:
            if person.is_repeat():
                first_session = person.first_significant_session()
                repeat_session = person.first_repeat_session()
                diff = repeat_session.started_at - first_session.ended_at
                key = str(person.created_at.date())
                if resolution == 'monthly':
                    key = calendar.month_name[person.created_at.month]
                elif resolution == 'weekly':
                    key = person.created_at.isocalendar()[1]
                if key in data:
                    data[key].append(diff.seconds / 3600.0)
                else:
                    data[key] = [diff.seconds / 3600.0]

        # calculate total for each resolution unit ---
        report = OrderedDict(sorted(data.items()))
        for date in report.keys():
            if len(report[date]) > 0:
                report[date] = sum(report[date]) / float(len(report[date]))
            else:
                report[date] = 0.0

        return Response(report, status=status.HTTP_200_OK)


class SessionReportViewSet(viewsets.ViewSet):
    def generate_query(self, request):
        default_ranges = {
            'daily': datetime.timedelta(days=30),
            'weekly': datetime.timedelta(weeks=13),
            'monthly': datetime.timedelta(days=365)
        }
        resolution = self.request.query_params.get('resolution', 'weekly')
        market = self.request.query_params.get('market', None)
        end = self.request.query_params.get('end', datetime.datetime.now())
        begin = self.request.query_params.get(
            'begin', end - default_ranges[resolution])
        excl_non_paying = self.request.query_params.get(
            'exclude_non_payers', True) == 'True'

        query = [Q(ended_at__lte=end), Q(started_at__gte=begin)]
        if market is not None:
            query.append(Q(bike__market=market))
        if excl_non_paying:
            query.append(Q(renter__paying_customer=True))

        return models.Session.objects\
            .select_related('renter')\
            .exclude(ended_at=None)\
            .filter(reduce(operator.and_, query))\
            .order_by('ended_at')\
            .all()


class AverageSessionsPerActivePersonReportViewSet(SessionReportViewSet):
    def list(self, request):
        # Of people active in the last four weeks,
        # how many times did they rent - on average - this week
        market = self.request.query_params.get('market', None)
        active_within = int(self.request.query_params.get('active_within', 14))
        end = self.request.query_params.get('end', datetime.datetime.now())
        begin = self.request.query_params.get(
            'begin', end - datetime.timedelta(days=365))
        excl_non_paying = self.request.query_params.get(
            'exclude_non_payers', True) == 'True'

        query = [Q(ended_at__lte=end), Q(started_at__gte=begin)]
        if market is not None:
            query.append(Q(bike__market=market))
        if excl_non_paying:
            query.append(Q(renter__paying_customer=True))

        sessions = models.Session.objects\
            .select_related('renter')\
            .exclude(ended_at=None)\
            .filter(reduce(operator.and_, query))\
            .order_by('ended_at')\
            .all()

        # for each date, create a key for each person who has been active in
        # the past 4 weeks.
        data = OrderedDict()
        for date in date_util.date_list(begin.date(), end.date()):
            date_data = {}
            four_weeks_ago = date - datetime.timedelta(days=active_within)
            for session in sessions:
                if session.ended_at.date() <= date and\
                   session.ended_at.date() > four_weeks_ago:
                    if session.renter.guid in date_data and\
                       session.ended_at.date() == date:
                        date_data[session.renter.guid] += 1
                    elif session.ended_at.date() == date:
                        date_data[session.renter.guid] = 1
                    else:
                        date_data[session.renter.guid] = 0
            data[str(date)] = date_data

        report = OrderedDict()
        for date in data.keys():
            if len(data[date].values()) > 0:
                report[date] = sum(data[date].values()) /\
                               float(len(data[date].values()))
            else:
                report[date] = 0.0

        return Response(report, status=status.HTTP_200_OK)


class AverageSessionDurationReportViewSet(SessionReportViewSet):
    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        sessions = super(AverageSessionDurationReportViewSet,
                         self).generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for session in sessions:
            delta = (session.ended_at - session.started_at).seconds
            key = str(session.ended_at.date())
            if resolution == 'monthly':
                key = calendar.month_name[session.ended_at.month]
            elif resolution == 'weekly':
                key = session.ended_at.isocalendar()[1]
            if key in data:
                data[key].append(delta)
            else:
                data[key] = [delta]

        # calculate average for each resolution unit ---
        report = OrderedDict()
        for key in data.keys():
            total = 0
            for datapoint in data[key]:
                total += datapoint
            report[key] = total / len(data[key])

        return Response(report, status=status.HTTP_200_OK)


class AverageSessionDistanceReportViewSet(SessionReportViewSet):
    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        sessions = super(AverageSessionDistanceReportViewSet,
                         self).generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for session in sessions:
            distance = session.total_distance_miles
            key = str(session.ended_at.date())
            if resolution == 'monthly':
                key = calendar.month_name[session.ended_at.month]
            elif resolution == 'weekly':
                key = session.ended_at.isocalendar()[1]
            if key in data:
                data[key].append(distance)
            else:
                data[key] = [distance]

        # calculate average for each resolution unit ---
        report = OrderedDict()
        for key in data.keys():
            total = 0
            for datapoint in data[key]:
                total += datapoint
            report[key] = total / len(data[key])

        return Response(report, status=status.HTTP_200_OK)


class AverageSessionValueReportViewSet(SessionReportViewSet):
    def list(self, request):
        resolution = self.request.query_params.get('resolution', 'weekly')
        sessions = super(AverageSessionValueReportViewSet,
                         self).generate_query(request)

        # gather data for each resolution unit ---
        data = OrderedDict()
        for session in sessions:
            price = session.transaction.total_amount
            key = str(session.ended_at.date())
            if resolution == 'monthly':
                key = calendar.month_name[session.ended_at.month]
            elif resolution == 'weekly':
                key = session.ended_at.isocalendar()[1]
            if key in data:
                data[key].append(price)
            else:
                data[key] = [price]

        # calculate average for each resolution unit ---
        report = OrderedDict()
        for key in data.keys():
            total = 0
            for datapoint in data[key]:
                if datapoint is not None:
                    total += datapoint
            report[key] = total / len(data[key])

        return Response(report, status=status.HTTP_200_OK)


class SessionDurationDistributionReportViewSet(viewsets.ViewSet):
    def list(self, request):
        params = request.query_params
        session_report = reports.SessionReport(**params)
        return Response(
            session_report.duration_distribution(),
            status=status.HTTP_200_OK)


class MixpanelEventsViewSet(viewsets.ViewSet):
    def list(self, request):
        from_date = request.query_params.get('from_date')
        to_date = request.query_params.get('to_date')
        events = request.query_params.get('events')
        where = request.query_params.get('where')
        mixpanel_events = mixpanel.events(from_date, to_date, events, where)
        return Response(mixpanel_events, status=status.HTTP_200_OK)
