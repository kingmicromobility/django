"""
Django settings for baas_backend project on Heroku. Fore more info, see:
https://github.com/heroku/heroku-django-template

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

import os
import sys
import braintree
import datetime
import dj_database_url

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY', "ok7s(k5!c_nj_!l(sd3+cp1jdh=9apx%ai33=ut7x*q@tpmw(n")

DEBUG = os.environ.get('DEBUG', True)

# Application definition
INSTALLED_APPS = (
    'corsheaders',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django_nose',
    'django_rq',
    'djrill',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'custom_user',
    'baas',
    'reporting',
)

REDIS_URL = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': '%s/0' % REDIS_URL,
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'CONNECTION_POOL_KWARGS': {'max_connections': 5},
            'MAX_ENTRIES': 5000,
        },
    },
}

RQ_QUEUES = {
    'high': {
        'USE_REDIS_CACHE': 'default',
    },
    'default': {
        'USE_REDIS_CACHE': 'default',
    },
    'low': {
        'USE_REDIS_CACHE': 'default',
    },
}

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,

    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'
}

JWT_AUTH = {
    'JWT_ALLOW_REFRESH': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=182),
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=365),
    'JWT_AUTH_HEADER_PREFIX': 'Token',
    'JWT_RESPONSE_PAYLOAD_HANDLER':
        'baas_backend.auth_payload.jwt_response_payload_handler',
}

MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

CORS_ORIGIN_REGEX_WHITELIST = (
    '^.*baasbikes\.com$',
    '^.*baas\.bike$',
    '^.*baasbikes\.dev$',
    '^.*baas\.dev$',
    '.*',
)

FILE_UPLOAD_HANDLERS = (
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
)

ROOT_URLCONF = 'baas_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': True,
            'libraries': {
                'staticfiles': 'django.templatetags.static',
            },
        },
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s %(levelname)s %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
            'formatter': 'default',
        }
    },
    'loggers': {
        "django": {
            "handlers": ["console"],
            "level": "INFO"
        },
        'baas_debug': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
}

WSGI_APPLICATION = 'baas_backend.wsgi.application'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
AUTH_USER_MODEL = 'custom_user.EmailUser'
LOGIN_URL = '/accounts/login/'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 6,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Database

DATABASES = {
    'default': dj_database_url.config(default=os.environ.get('DATABASE_URL'))
}
POSTGIS_VERSION = (3, 0, 0)
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'


GEOS_LIBRARY_PATH = os.environ.get('GEOS_LIBRARY_PATH')
GDAL_LIBRARY_PATH = os.environ.get('GDAL_LIBRARY_PATH')
PROJ4_LIBRARY_PATH = os.environ.get('PROJ4_LIBRARY_PATH')

if GEOS_LIBRARY_PATH:
    GEOS_LIBRARY_PATH = "%s/libgeos_c.so" % os.environ.get('GEOS_LIBRARY_PATH')
if GDAL_LIBRARY_PATH:
    GDAL_LIBRARY_PATH = "%s/libgdal.so" % os.environ.get('GDAL_LIBRARY_PATH')
if PROJ4_LIBRARY_PATH:
    PROJ4_LIBRARY_PATH = "%s/libproj.so" % os.environ.get('PROJ4_LIBRARY_PATH')

MANDRILL_API_KEY = os.environ.get('MANDRILL_API_KEY')
EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
DEFAULT_FROM_EMAIL = "hello@baasbikes.com"

EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_HOST_USER = 'rob@baasbikes.com'
EMAIL_HOST_PASSWORD = os.environ.get('MANDRILL_API_KEY')
EMAIL_PORT = 587
EMAIL_USE_TLS = False

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# -----------------------
# Static Files
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(os.path.dirname(__file__), 'static'),
)
# https://warehouse.python.org/project/whitenoise/
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
# -----------------------

# Use nose to run all tests
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

# Tell nose to measure coverage on the 'foo' and 'bar' apps
NOSE_ARGS = [
    '--with-coverage',
    '--cover-package=baas',
    '--cover-inclusive'
]


# Braintree configuration
braintree_merchant_id = ''
braintree_public_key = ''
braintree_private_key = ''
braintree_environment = ''

if os.environ.get('BRAINTREE_MERCHANT_ID') is not None:
    braintree_merchant_id = os.environ.get('BRAINTREE_MERCHANT_ID')
if os.environ.get('BRAINTREE_PUBLIC_KEY') is not None:
    braintree_public_key = os.environ.get('BRAINTREE_PUBLIC_KEY')
if os.environ.get('BRAINTREE_PRIVATE_KEY') is not None:
    braintree_private_key = os.environ.get('BRAINTREE_PRIVATE_KEY')
if os.environ.get('BRAINTREE_ENVIRONMENT') is not None:
    braintree_environment = os.environ.get('BRAINTREE_ENVIRONMENT')

if braintree_environment == 'production':
    braintree.Configuration.configure(
        braintree.Environment.Production,
        merchant_id=braintree_merchant_id,
        public_key=braintree_public_key,
        private_key=braintree_private_key)
else:
    braintree.Configuration.configure(
        braintree.Environment.Sandbox,
        merchant_id=braintree_merchant_id,
        public_key=braintree_public_key,
        private_key=braintree_private_key)
