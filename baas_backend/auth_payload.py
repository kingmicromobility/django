from baas import serializers


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'person': serializers.PersonSerializer(user.person_set.first()).data
    }
