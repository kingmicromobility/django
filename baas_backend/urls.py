from django.conf.urls import include, url
from django.contrib import admin

import django_rq.urls
import rest_framework.urls
from rest_framework_swagger.views import get_swagger_view
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from rest_framework_nested import routers
from baas import views
from reporting import views as reporting_views

# default_router = original_routers.DefaultRouter()

router = routers.DefaultRouter()
router.register(r'users',           views.UserViewSet)
router.register(r'groups',          views.GroupViewSet)
router.register(r'people',          views.PersonViewSet,      'Person')
router.register(r'bikes',           views.BikeViewSet,        'Bike')
router.register(r'bike_brands',     views.BikeBrandViewSet)
router.register(r'bike_detail',     views.BikeDetailViewSet)
router.register(r'bike_sizes',      views.BikeSizeViewSet)
router.register(r'bike_styles',     views.BikeStyleViewSet)
router.register(r'lock_events',     views.LockEventViewSet)
router.register(r'markets',         views.MarketViewSet,      'Market')
router.register(r'reservations',    views.ReservationViewSet, 'Reservation')
router.register(r'sessions',        views.SessionViewSet,     'Session')
router.register(r'subscribers',     views.SubscriberViewSet,  'Subscriber')
router.register(r'zendesk_tickets', views.ZendeskTicketViewSet, 'ZendeskTickets')
router.register(r'trips',           views.TripViewSet,        'Trip')
router.register(r'feedback_events', views.FeedbackEventViewSet)
router.register(r'answer_codes',    views.AnswerCodesViewSet, 'AnswerCode')
router.register(r'feedback_prompt_sets', views.FeedbackPromptSetViewSet)
router.register(r'payments/client', views.PaymentViewSet,     'Payments')
router.register(
    r'temporary_password',
    views.TemporaryPasswordViewSet,
    'TemporaryPassword')
router.register(
    r'reports/average_session_duration',
    reporting_views.AverageSessionDurationReportViewSet,
    'AvgSessionDurationReport')
router.register(
    r'reports/session_duration_distribution',
    reporting_views.SessionDurationDistributionReportViewSet,
    'SessionDurationDistributionReport')
router.register(
    r'reports/average_session_distance',
    reporting_views.AverageSessionDistanceReportViewSet,
    'AvgSessionDistanceReport')
router.register(
    r'reports/average_session_value',
    reporting_views.AverageSessionValueReportViewSet,
    'AvgSessionValueReport')
router.register(
    r'reports/average_sessions_per_active_person',
    reporting_views.AverageSessionsPerActivePersonReportViewSet,
    'AvgSessionsPerActivePersonReport')
router.register(
    r'reports/new_signup_count',
    reporting_views.NewSignupCountReportViewSet,
    'NewSignupCountReport')
router.register(
    r'reports/new_rider_count',
    reporting_views.NewRiderCountReportViewSet,
    'NewRiderCountReport')
router.register(
    r'reports/new_repeat_count',
    reporting_views.NewRepeatRiderCountReportViewSet,
    'NewRepeatCountReport')
router.register(
    r'reports/time_to_first_ride',
    reporting_views.TimeToFirstRideReportViewSet,
    'TimeToFirstRideReport')
router.register(
    r'reports/time_to_repeat_ride',
    reporting_views.TimeToRepeatRideReportViewSet,
    'TimeToRepeatRideReport')
router.register(
    r'reports/mixpanel_events',
    reporting_views.MixpanelEventsViewSet,
    'MixpanelEvents')

people_router = routers.NestedSimpleRouter(router, r'people', lookup='person')
people_router.register(
    r'payment_methods',
    views.PaymentMethodViewSet,
    basename='person-payment-methods')
people_router.register(
    r'trips',
    views.PersonTripViewSet,
    basename='person-trip')
people_router.register(
    r'sessions',
    views.PersonSessionViewSet,
    basename='person-session')
people_router.register(
    r'reservations',
    views.PersonReservationViewSet,
    basename='person-reservation')

markets_router = routers.NestedSimpleRouter(router, r'markets', lookup='market')
markets_router.register(
    r'contains',
    views.MarketContentsViewSet,
    basename='market-contents')
markets_router.register(
    r'sessions',
    views.MarketSessionsViewSet,
    basename='market-sessions')

bikes_router = routers.NestedSimpleRouter(router, r'bikes', lookup='bike')
bikes_router.register(
    r'trips',
    views.BikeTripViewSet,
    basename='bike-trip')
bikes_router.register(
    r'sessions',
    views.BikeSessionViewSet,
    basename='bike-sesssion')

swagger_view = get_swagger_view(title='Baas Bikes API')

urlpatterns = [
    url(r'^',                   include(router.urls)),
    url(r'^',                   include(people_router.urls)),
    url(r'^',                   include(bikes_router.urls)),
    url(r'^',                   include(markets_router.urls)),
    url(r'^api-auth/',          include(rest_framework.urls, namespace='rest_framework')),
    url(r'^api-auth-token/',    obtain_jwt_token),
    url(r'^api-refresh-token/', refresh_jwt_token),
    url(r'^api-verify-token/',  verify_jwt_token),
    url(r'^admin/',             admin.site.urls),
    url(r'^docs/',              swagger_view),
    url(r'^django-rq/',         include(django_rq.urls))
]
